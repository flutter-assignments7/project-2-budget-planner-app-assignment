

import 'package:budget_planner_app/models/db_table.dart';

class Currency {
  late int id;
  late String nameEn;
  late String nameAr;

  static const TABLE_NAME = 'currencies';
  Currency({
    required this.id,
    required this.nameAr,
    required this.nameEn,
  });


  Currency.fromMap(Map<String, dynamic> rowMap) {
    id = rowMap['id'];
    nameEn = rowMap['name_en'];
    nameEn = rowMap['name_ar'];
  }

  @override
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['name_en'] = nameEn;
    map['name_ar'] = nameAr;
    return map;
  }

  // @override
  // set tableName(String _tableName) {
  //   // TODO: implement tableName
  //   super.tableName = 'currencies';
  // }
}
