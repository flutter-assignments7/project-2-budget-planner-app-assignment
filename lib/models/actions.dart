


class ActionOperation{
  late int id;
  late int amount;
  late DateTime date;
  late bool? expense;
  late String notes;
  late int userId;
  late int categoryId;
  late int currencyId;

  static const TABLE_NAME = 'actions';

  ActionOperation({
    required this.amount,
    required this.expense,
    required this.date,
    required this.notes,
    required this.categoryId,
    required this.currencyId,
    required this.userId,
  });

  ActionOperation.fromMap(Map<String, dynamic> rowMap){
    id = rowMap['id'];
    amount = rowMap['amount'];
    date = DateTime.parse(rowMap['date']);
    expense = rowMap['expense'] == 1;
    notes = rowMap['notes'];
    userId = rowMap['user_id'];
    categoryId = rowMap['category_id'];
    currencyId = rowMap['currency_id'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['amount'] = amount;
    map['date'] = date.toIso8601String();
    map['expense'] = expense! ? 1 : 0;
    map['notes'] = notes;
    map['user_id'] = userId;
    map['category_id'] = categoryId;
    map['currency_id'] = currencyId;
    return map;
  }
}