import 'package:budget_planner_app/models/user.dart';
import 'package:budget_planner_app/notifiers/actions_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/user_provider_notifier.dart';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/helpers.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_button.dart';
import 'package:budget_planner_app/widgets/budget_planner_card.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/budget_planner_textField.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with Helpers{


  late TapGestureRecognizer _tapGestureRecognizer;

  late TextEditingController _emailTextEditingController;
  late TextEditingController _codeTextEditingController;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tapGestureRecognizer = TapGestureRecognizer()..onTap = navigateToRegister;
    _emailTextEditingController = TextEditingController();
    _codeTextEditingController = TextEditingController();


  }


  @override
  void dispose() {
    // TODO: implement dispose
    _emailTextEditingController.dispose();
    _codeTextEditingController.dispose();
    _tapGestureRecognizer.dispose();
    super.dispose();
  }

  void navigateToRegister() =>
      Navigator.pushNamed(context, '/create_account_screen');

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsetsDirectional.only(
            start: SizeConfig.scaleWidth(20),
            end: SizeConfig.scaleWidth(20),
            top: SizeConfig.scaleHeight(81),
          ),
          child: Column(
            children: [
              BudgetPlannerCard(
                widthCard: 120,
                heightCard: 120,
                pathImage: 'images/login_icon.png',
                borderRadiusCard: 30,
                widthImageCard: 56.81,
                heightImageCard: 52.93,
              ),
              SizedBox(height: SizeConfig.scaleHeight(13)),
              BudgetPlannerText(
                text: AppLocalizations.of(context)!.login_title,
                color: Color(0xFF0D0E56),
                fontsize: 20,
                textAlign: TextAlign.center,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(height: SizeConfig.scaleHeight(11)),
              BudgetPlannerText(
                text: AppLocalizations.of(context)!.login_sup_title,
                color: Color(0xFF7B7C98),
                fontsize: 15,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: SizeConfig.scaleHeight(50)),
              BudgetPlannerTextField(
                textEditingController: _emailTextEditingController,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      // color:
                      color: Color(0xFF000000).withAlpha(20),
                      offset: Offset(0, 0),
                      blurRadius: 6,
                      spreadRadius: 1,
                    ),
                  ],
                ),
                hintTextField: AppLocalizations.of(context)!.login_hint_email,
                keyboardType: TextInputType.emailAddress,
              ),
              SizedBox(height: SizeConfig.scaleHeight(15)),
              BudgetPlannerTextField(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      // color:
                      color: Color(0xFF000000).withAlpha(20),
                      offset: Offset(0, 0),
                      blurRadius: 6,
                      spreadRadius: 1,
                    ),
                  ],
                ),
                hintTextField:
                    AppLocalizations.of(context)!.login_hint_pin_code,
                keyboardType: TextInputType.number,
                textEditingController: _codeTextEditingController,
              ),
              SizedBox(height: SizeConfig.scaleHeight(15)),
              BudgetPlannerButton(
                onPressedPage: () async {
                  await performLogin();
                },
                textButton: AppLocalizations.of(context)!.login_text_btn,
              ),
              SizedBox(height: SizeConfig.scaleHeight(20)),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text:
                      AppLocalizations.of(context)!.login_text_don_not_account,
                  style: TextStyle(
                    color: Color(0xFF7B7C98),
                    fontFamily: 'Montserrat',
                    fontSize: SizeConfig.scaleTextFont(15),
                  ),
                  children: [
                    TextSpan(
                      recognizer: _tapGestureRecognizer,
                      text: AppLocalizations.of(context)!
                          .login_text_create_account,
                      style: TextStyle(
                        color: Color(0xFF351DB6),
                        fontSize: SizeConfig.scaleTextFont(15),
                        fontFamily: 'Montserrat',
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }


  Future performLogin() async {
    if (checkData()) {
      await login();
    }
  }

  bool checkData() {
    if (_emailTextEditingController.text.isNotEmpty &&
        _codeTextEditingController.text.isNotEmpty) {
      return true;
    }
    showSnackBar(context, message: 'Enter required data!',error: true);
    return false;
  }



  Future login() async {
    bool created =
    await Provider.of<UserProviderNotifier>(context,listen: false).login(userData: user);
    if (created) {
      print('my Pin ');
      Navigator.pushNamedAndRemoveUntil(
          context, '/main_screen', (route) => false);
    }
  }


  User get user {
    User user = User();
    user.email = _emailTextEditingController.text;
    user.pin = int.parse(_codeTextEditingController.text);
    return user;
  }

}
