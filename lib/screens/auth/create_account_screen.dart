import 'dart:convert';

import 'package:budget_planner_app/models/user.dart';
import 'package:budget_planner_app/notifiers/currencyDetails.dart';
import 'package:budget_planner_app/notifiers/user_provider_notifier.dart';
import 'package:budget_planner_app/screens/auth/create_account_success.dart';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/helpers.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_button.dart';
import 'package:budget_planner_app/widgets/budget_planner_card.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/create_account_card_row.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class CreateAccountScreen extends StatefulWidget {
  const CreateAccountScreen({Key? key}) : super(key: key);

  @override
  _CreateAccountScreenState createState() => _CreateAccountScreenState();
}

class _CreateAccountScreenState extends State<CreateAccountScreen>
    with Helpers {
  Currencies? currency;

  late TextEditingController _nameTextController;
  late TextEditingController _emailTextController;
  late TextEditingController _dailyLimitTextController;

  @override
  void initState() {
    // TODO: implement initState
    _nameTextController = TextEditingController();
    _emailTextController = TextEditingController();
    _dailyLimitTextController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameTextController.dispose();
    _emailTextController.dispose();
    _dailyLimitTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // final currencyPV = Provider.of<CurrencyList>(context);
    // currencyPV.getSelectedCurrencyName().isNotEmpty
    //     ? currency!.name //currencyTextController.text=currencyPV.cList[currencyPV.getSelectedIndex()].name //currencies[currencyPV.getSelectedIndex()].nameEn
    //     :'';

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: Color(0xFF472FC8)),
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              BudgetPlannerCard(
                widthCard: 120,
                heightCard: 120,
                pathImage: 'images/login_icon.png',
                borderRadiusCard: 30,
                widthImageCard: 56.81,
                heightImageCard: 52.93,
              ),
              SizedBox(height: SizeConfig.scaleHeight(13)),
              BudgetPlannerText(
                text: AppLocalizations.of(context)!.create_account_title,
                color: Color(0xFF0D0E56),
                fontsize: 20,
                textAlign: TextAlign.center,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(height: SizeConfig.scaleHeight(11)),
              BudgetPlannerText(
                text: AppLocalizations.of(context)!.create_account_supTitle,
                color: Color(0xFF7B7C98),
                fontsize: 15,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: SizeConfig.scaleHeight(21)),
              Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(
                        SizeConfig.scaleWidth(10))),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.scaleWidth(15),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CreateAccountCardRow(
                        controller: _nameTextController,
                        textInputType: TextInputType.text,
                        title: AppLocalizations.of(context)!
                            .create_account_text_field_name,
                        hintTextField: AppLocalizations.of(context)!
                            .create_account_text_field_name_hint,
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      CreateAccountCardRow(
                        controller: _emailTextController,
                        textInputType: TextInputType.emailAddress,
                        title: AppLocalizations.of(context)!
                            .create_account_text_field_email_address,
                        hintTextField: AppLocalizations.of(context)!
                            .create_account_text_field_name_hint,
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      SizedBox(height: SizeConfig.scaleHeight(20)),
                      Row(
                        children: [
                          BudgetPlannerText(
                            text: AppLocalizations.of(context)!
                                .create_account_text_field_currency,
                            fontWeight: FontWeight.w500,
                            fontsize: SizeConfig.scaleTextFont(15),
                            color: Color(0xFF181819),
                          ),
                          Spacer(),
                          BudgetPlannerText(
                            text: currency?.name ?? '0000',
                            fontsize: SizeConfig.scaleTextFont(15),
                            color: Color(0xFF7B7C98),
                          ),
                          SizedBox(width: SizeConfig.scaleWidth(2)),
                          IconButton(
                            padding: EdgeInsetsDirectional.all(0),
                            iconSize: SizeConfig.scaleWidth(15),
                            alignment: Alignment.center,
                            constraints: BoxConstraints(),
                            onPressed: () async {
                              var resultCurrency = await Navigator.pushNamed(
                                  context, '/currency_select_screen');
                              setState(() {
                                currency = resultCurrency as Currencies;
                                print(currency);
                              });
                            },
                            icon: Icon(
                              Icons.arrow_forward_ios,
                              color: Color(0xFF555568),
                              size: SizeConfig.scaleWidth(17),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: SizeConfig.scaleHeight(20)),
                      Divider(
                        thickness: 1,
                      ),
                      CreateAccountCardRow(
                        textInputType: TextInputType.number,
                        controller: _dailyLimitTextController,
                        title: AppLocalizations.of(context)!
                            .create_account_text_field_daily_limit,
                        hintTextField: AppLocalizations.of(context)!
                            .create_account_text_field_daily_limit_hint,
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      SizedBox(height: SizeConfig.scaleHeight(20)),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pushNamed('/pin_code_screen');
                        },
                        child: Container(
                          width: double.infinity,
                          child: BudgetPlannerText(
                            text: AppLocalizations.of(context)!
                                .create_account_text_field_your_pin,
                            fontWeight: FontWeight.w500,
                            fontsize: SizeConfig.scaleTextFont(15),
                            color: Color(0xFF181819),
                          ),
                        ),
                      ),
                      SizedBox(height: SizeConfig.scaleHeight(20)),
                    ],
                  ),
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(102)),
              BudgetPlannerButton(
                onPressedPage: () async {
                  await performCreateAccount();
                },
                textButton:
                    AppLocalizations.of(context)!.create_account_btn_text,
                color: Color(0xFF472FC8),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future performCreateAccount() async {
    if (checkData()) {
      await createAccount();
    }
  }

  bool checkData() {
    if (_nameTextController.text.isNotEmpty &&
        _emailTextController.text.isNotEmpty &&
        _dailyLimitTextController.text.isNotEmpty &&
        currency != null &&
        SharedPrefController().preferences.containsKey('xp')) {
      return true;
    }
    showSnackBar(context, message: 'Enter required data!', error: true);
    return false;
  }

  Future createAccount() async {
    bool created =
        await Provider.of<UserProviderNotifier>(context, listen: false)
            .create(user);
    if (created) {
      print(user.currencyId);
      Navigator.pushNamedAndRemoveUntil(
          context, '/create_account_success', (route) => false);
    }
  }

  User get user {
    User user = User();
    user.name = _nameTextController.text;
    user.email = _emailTextController.text;
    user.currencyId = currency!.id;
    user.dayLimit = double.parse(_dailyLimitTextController.text.trim().replaceAll(' ', ' '));
    user.pin = SharedPrefController().preferences.getInt('xp')??0;
    return user;
  }
}
