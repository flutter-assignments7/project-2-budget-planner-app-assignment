import 'dart:ui';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_button.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/pin_button.dart';
import 'package:budget_planner_app/widgets/pin_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PinCodeScreen extends StatefulWidget {
  static const routeName = '/login-screen';

  @override
  _PinCodeScreenState createState() => _PinCodeScreenState();
}

class _PinCodeScreenState extends State<PinCodeScreen> {

  String _password = '';
  String _newPassword = '';
  String _newPassword2 = '';


  String message = '';
  bool showMessage = false;
  Color messageColor=Colors.white;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      extendBody: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Color(0xffF472FCB)),
        elevation: 0,
      ),
      body: Column(
        children: [
          BudgetPlannerText(
            text: AppLocalizations.of(context)!.pin_code_title,
            color: Color(0xFF0D0E56),
            fontsize: 20,
            textAlign: TextAlign.center,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: SizeConfig.scaleHeight(5)),
          BudgetPlannerText(
            text: AppLocalizations.of(context)!.pin_code_supTitle,
            color: Color(0xFF7B7C98),
            fontsize: 15,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: SizeConfig.scaleHeight(45)),
          SizedBox(height:SizeConfig.scaleHeight(22),child: Visibility(visible: showMessage,child: BudgetPlannerText(text: message,color: messageColor,fontsize: 18,fontWeight: FontWeight.bold))),
          SizedBox(height: SizeConfig.scaleHeight(20)),
          PinContainer(passCode: _password,backSpaceFunction:_backSpaceFunction),
          SizedBox(height: SizeConfig.scaleHeight(60)),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  PinButton(
                    text: '1',
                    onPressed: _password.length > 3
                        ? null
                        : () {
                      _numberFunction('1');
                    },
                  ),
                  PinButton(
                    text: '2',
                    onPressed: _password.length > 3
                        ? null
                        : () {
                      _numberFunction('2');
                    },
                  ),
                  PinButton(
                    text: '3',
                    onPressed: _password.length > 3
                        ? null
                        : () {
                      _numberFunction('3');
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  PinButton(
                    text: '4',
                    onPressed: _password.length > 3
                        ? null
                        : () {
                      _numberFunction('4');
                    },
                  ),
                  PinButton(
                    text: '5',
                    onPressed: _password.length > 3
                        ? null
                        : () {
                      _numberFunction('5');
                    },
                  ),
                  PinButton(
                    text: '6',
                    onPressed: _password.length > 3
                        ? null
                        : () {
                      _numberFunction('6');
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  PinButton(
                    text: '7',
                    onPressed: _password.length > 3
                        ? null
                        : () {
                      _numberFunction('7');
                    },
                  ),
                  PinButton(
                    text: '8',
                    onPressed: _password.length > 3
                        ? null
                        : () {
                      _numberFunction('8');
                    },
                  ),
                  PinButton(
                    text: '9',
                    onPressed: _password.length > 3
                        ? null
                        : () {
                      _numberFunction('9');
                    },
                  ),
                ],
              ),
            ],
          ),
          Spacer(),
          Container(
            margin: EdgeInsets.only(
              left: SizeConfig.scaleWidth(20),
              right: SizeConfig.scaleWidth(20),
              bottom: SizeConfig.scaleHeight(53),
            ),

            child: BudgetPlannerButton(
                textButton: AppLocalizations.of(context)!.pin_code_text_btn,
                onPressedPage:_password.length < 4
                    ? null
                    : _applyButtonFunction,
                color:Color(0xff351DB6)),
          ),
        ],
      ),
    );
  }

  void _numberFunction(String value) {
    setState(() {
      _password += value;
    });
  }
  void _backSpaceFunction(){
    setState(() {
      _password = _password.substring(0, _password.length - 1);
    });
  }
  void _applyButtonFunction(){
    if (_newPassword.length == 0) {
      _newPassword = _password;
      setState(() {
        showMessage = true;
        messageColor = Color(0xffF472FCB);
        message = 'Repeat your passcode';
        _password = '';
      });
    } else {
      setState(() {
        _newPassword2 = _password;
      });
    }
    if (_newPassword.length == 4 && _newPassword2.length == 4) {
      if (_newPassword != _newPassword2) {
        setState(() {
          showMessage = true;
          messageColor = Colors.red;
          message =
          'Passcodes don\'t match, try again';
          _password = '';
          _newPassword = '';
          _newPassword2 = '';
        });
      } else {
        //final password after verify//
        SharedPrefController().preferences.setInt('xp',int.parse(_password));
        Navigator.pop(context);
      }
    }
  }

}
