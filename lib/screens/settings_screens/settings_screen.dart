import 'package:budget_planner_app/notifiers/actions_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/user_provider_notifier.dart';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/helpers.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/home_screen_list_tile.dart';
import 'package:budget_planner_app/widgets/profile_screen_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> with Helpers {
  @override
  Widget build(BuildContext context) {
    final userPV = Provider.of<UserProviderNotifier>(context);
    final actionsPV = Provider.of<ActionsProviderNotifier>(context,listen: false);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: BudgetPlannerText(
          text: AppLocalizations.of(context)!.settings_screen_title,
          color: Color(0xFF0D0E56),
          fontsize: SizeConfig.scaleTextFont(20),
          fontWeight: FontWeight.bold,
        ),
        centerTitle: true,
        iconTheme: IconThemeData(
          color: Color(0xFF472FC8),
        ),
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
          top: SizeConfig.scaleHeight(34),
        ),
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BudgetPlannerText(
                  text: AppLocalizations.of(context)!.settings_screen_general,
                  color: Color(0xFF0D0E56),
                  fontsize: 15,
                  fontWeight: FontWeight.w500,
                ),
                SizedBox(height: SizeConfig.scaleHeight(15)),
                ProfileScreenCard(
                  onTap: () {
                    Navigator.pushNamed(context, '/about_app_screen');
                  },
                  titleColor: Color(0xFF0D0E56),
                  title:
                      AppLocalizations.of(context)!.settings_screen_about_app,
                  leading: Icon(
                    Icons.info,
                    color: Color(0xFF0D0E56),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.black,
                    size: SizeConfig.scaleWidth(20),
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(10)),
                ProfileScreenCard(
                  titleColor: Color(0xFF0D0E56),
                  title: AppLocalizations.of(context)!.settings_screen_language,
                  leading: Icon(
                    Icons.language,
                    color: Color(0xFF0D0E56),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.black,
                    size: SizeConfig.scaleWidth(20),
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(10)),
                ProfileScreenCard(
                  onTap: () {
                    logout();
                  },
                  titleColor: Color(0xFF0D0E56),
                  title: AppLocalizations.of(context)!.settings_screen_logout,
                  leading: Icon(
                    Icons.logout,
                    color: Color(0xFF0D0E56),
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(25)),
                BudgetPlannerText(
                  text: AppLocalizations.of(context)!
                      .settings_screen_account_data,
                  color: Color(0xFF0D0E56),
                  fontsize: 15,
                  fontWeight: FontWeight.w500,
                ),
                SizedBox(height: SizeConfig.scaleHeight(15)),
                ProfileScreenCard(
                  titleColor: Color(0xFFD50000),
                  title:
                      AppLocalizations.of(context)!.settings_screen_clear_data,
                  leading: Icon(
                    Icons.delete_forever,
                    color: Color(0xFFD50000),
                  ),
                  onTap: () async {
                    showDialog(
                      context: context,
                      builder: (context) => showAlertDialog(
                        context,
                        title: AppLocalizations.of(context)!
                            .settings_screen_clear_data,
                        subtitle: AppLocalizations.of(context)!
                            .settings_screen_clear_data_subtitle,
                        confirmFun: ()async{
                         await actionsPV.delete(SharedPrefController().preferences.getInt('id')!);
                         showSnackBar(context,message: 'Data cleared');
                         Navigator.pushReplacementNamed(context, '/main_screen');

                        },
                      ),
                    );
                  },
                  borderWidth: 1,
                  borderColor: Color(0xFFD50000),
                ),
                SizedBox(height: SizeConfig.scaleHeight(10)),
                Consumer(
                  builder: (
                    BuildContext context,
                    UserProviderNotifier value,
                    Widget? child,
                  ) {
                    return ProfileScreenCard(
                      titleColor: Colors.white,
                      title: AppLocalizations.of(context)!
                          .settings_screen_remove_account,
                      leading: Icon(
                        Icons.person_remove,
                        color: Colors.white,
                      ),
                      onTap: () async {
                        showDialog(
                          context: context,
                          builder: (context) => showAlertDialog(
                            context,
                            title: AppLocalizations.of(context)!
                                .settings_screen_remove_account,
                            subtitle: AppLocalizations.of(context)!
                                .settings_screen_remove_account_subtitle,
                            confirmFun: ()async{
                              await delete(SharedPrefController().preferences.getInt('id')!);

                                await actionsPV.delete(SharedPrefController().preferences.getInt('id')!);
                                Navigator.pushReplacementNamed(context, '/login_screen');
                            },
                          ),
                        );
                      },
                      cardColor: Color(0xFFD50000),
                    );
                  },
                ),
              ],
            ),
            Positioned(
              bottom: SizeConfig.scaleHeight(44),
              left: 0,
              right: 0,
              child: BudgetPlannerText(
                text: AppLocalizations.of(context)!.settings_screen_app_name,
                color: Color(0xFF0D0E56),
                fontWeight: FontWeight.w800,
                fontsize: 15,
                textAlign: TextAlign.center,
              ),
            ),
            Positioned(
              bottom: SizeConfig.scaleHeight(20),
              left: 0,
              right: 0,
              child: BudgetPlannerText(
                text: AppLocalizations.of(context)!.settings_screen_app_version,
                color: Color(0xFF0D0E56),
                fontWeight: FontWeight.w300,
                fontsize: 15,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  AlertDialog showAlertDialog(
    BuildContext context, {
    required String title,
    required String subtitle,
    VoidCallback? confirmFun,
  }) {
    return AlertDialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(
            color: AppColors.SETTING_DANGER,
            width: 1,
          )),
      title: BudgetPlannerText(
        text: title,
        fontsize: 18,
        fontWeight: FontWeight.w500,
        color: AppColors.SETTING_DANGER,
      ),
      content: BudgetPlannerText(
        text: subtitle,
        fontsize: 15,
        color: AppColors.TITLE,
      ),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: BudgetPlannerText(
            text: 'No',
            fontsize: 15,
            color: AppColors.TITLE,
            fontWeight: FontWeight.w500,
          ),
        ),
        TextButton(
          onPressed: confirmFun,
          child: BudgetPlannerText(
            text: 'Yes',
            fontsize: 15,
            color: AppColors.SETTING_DANGER,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );
  }


  Future delete(int id) async {
    bool deleted =
        await Provider.of<UserProviderNotifier>(context, listen: false)
            .delete(id);
    String message =
        deleted ? 'user deleted Failed' : 'user deleted successfully';
    showSnackBar(context, message: message, error: !deleted);
  }

  Future logout() async {
    bool status = await SharedPrefController().logout();
    if (status)
      Navigator.pushNamedAndRemoveUntil(
          context, '/login_screen', (route) => false);
  }
}
