import 'package:budget_planner_app/models/category.dart';
import 'package:budget_planner_app/notifiers/category_provider_notifier.dart';
import 'package:budget_planner_app/screens/bottom_navegationBar/add_new_operation.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/home_screen_list_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class CategoriesScreen2 extends StatefulWidget {
  const CategoriesScreen2({Key? key}) : super(key: key);

  @override
  _CategoriesScreen2State createState() => _CategoriesScreen2State();
}

class _CategoriesScreen2State extends State<CategoriesScreen2> {
  late Category category;
  late Category _category;

  @override
  Widget build(BuildContext context) {
    List<Category> categoriesExpanses =
        Provider.of<CategoryProviderNotifier>(context)
            .categories
            .where((Category category) => category.expense!)
            .toList();
    List<Category> categoriesIncome =
        Provider.of<CategoryProviderNotifier>(context)
            .categories
            .where((Category category) => !category.expense!)
            .toList();

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: BudgetPlannerText(
            text: AppLocalizations.of(context)!.categories_screen_2_title,
            fontsize: SizeConfig.scaleTextFont(20),
            fontWeight: FontWeight.bold,
            color: Color(0xFF0D0E56),
            textAlign: TextAlign.center,
          ),
          iconTheme: IconThemeData(
            color: Color(0xFF472FC8),
          ),
          actions: [
            IconButton(
              padding:
                  EdgeInsetsDirectional.only(end: SizeConfig.scaleWidth(20)),
              constraints: BoxConstraints(),
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                Icons.close,
                color: Color(0xFF7B7C98),
              ),
            ),
          ],
        ),
        body: Padding(
          padding: EdgeInsetsDirectional.only(
            start: SizeConfig.scaleWidth(20),
            end: SizeConfig.scaleWidth(20),
            top: SizeConfig.scaleHeight(20),
          ),
          child: Column(
            children: [
              Container(
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(
                    color: Color(0xFFF1F4FF),
                    borderRadius:
                        BorderRadius.circular(SizeConfig.scaleWidth(40))),
                child: TabBar(
                  // indicatorWeight: double.infinity,
                  indicator: BoxDecoration(
                      color: Color(0xFF472FC8),
                      borderRadius: BorderRadius.circular(40)),
                  indicatorColor: Color(0xFF472FC8),
                  unselectedLabelColor: Color(0xFF181819),
                  labelColor: Colors.white,
                  labelStyle: TextStyle(
                    fontSize: 13,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Montserrat',
                  ),
                  tabs: [
                    Tab(
                      text: AppLocalizations.of(context)!
                          .category_screen_tab_bar_expenses,
                    ),
                    Tab(
                      text: AppLocalizations.of(context)!
                          .category_screen_tab_bar_income,
                    ),
                  ],
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(20)),
              Expanded(
                child: Consumer<CategoryProviderNotifier>(
                  builder: (
                    BuildContext context,
                    CategoryProviderNotifier value,
                    Widget? child,
                  ) {
                    return TabBarView(
                      children: [
                        Card(
                          elevation: 3,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Padding(
                            padding: EdgeInsetsDirectional.only(
                              start: SizeConfig.scaleWidth(12),
                              end: SizeConfig.scaleWidth(12),
                              top: SizeConfig.scaleHeight(20),
                              bottom: SizeConfig.scaleHeight(5),
                            ),
                            child: ListView.builder(
                              itemCount: categoriesExpanses.length,
                              itemBuilder: (context, index) {
                                category = categoriesExpanses.elementAt(index);
                                return GestureDetector(
                                  onTap: () {
                                    _category =
                                        categoriesExpanses.elementAt(index);
                                    Navigator.pop(context,_category);

                                  },
                                  child: Column(
                                    children: [
                                      HomeScreenListTile(
                                        fontWeight: FontWeight.w500,
                                        title: category.name,
                                      ),
                                      Divider(
                                        thickness: 0.3,
                                        indent: SizeConfig.scaleWidth(12),
                                        endIndent: SizeConfig.scaleWidth(12),
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                        Card(
                          elevation: 4,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Padding(
                            padding: EdgeInsetsDirectional.only(
                              start: SizeConfig.scaleWidth(12),
                              end: SizeConfig.scaleWidth(12),
                              top: SizeConfig.scaleHeight(20),
                              bottom: SizeConfig.scaleHeight(5),
                            ),
                            child: ListView.builder(
                                itemCount: categoriesIncome.length,
                                itemBuilder: (context, index) {
                                  category = categoriesIncome.elementAt(index);
                                  return GestureDetector(
                                    onTap: () {
                                      _category =
                                          categoriesIncome.elementAt(index);
                                      Navigator.pop(context,_category);
                                      // Navigator.pushReplacement(
                                      //   context,
                                      //   MaterialPageRoute(
                                      //     builder: (context) =>
                                      //         AddNewOperation.fromCategory(
                                      //             _category),
                                      //   ),
                                      // );
                                    },
                                    child: Column(
                                      children: [
                                        HomeScreenListTile(
                                          title: category.name,
                                          pathIcon: 'images/premium.png',
                                          fontWeight: FontWeight.w500,
                                        ),
                                        Divider(
                                          thickness: 0.3,
                                          indent: SizeConfig.scaleWidth(12),
                                          endIndent: SizeConfig.scaleWidth(12),
                                          height: 10,
                                        )
                                      ],
                                    ),
                                  );
                                }),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(10)),
            ],
          ),
        ),
      ),
    );
  }
}
