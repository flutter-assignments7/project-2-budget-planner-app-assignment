import 'package:budget_planner_app/models/actions.dart';
import 'package:budget_planner_app/models/currency.dart';
import 'package:budget_planner_app/notifiers/actions_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/category_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/currencyDetails.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/budget_planner_textField.dart';
import 'package:budget_planner_app/widgets/home_screen_list_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'details_screen.dart';

class AllActionScreen extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    final myActionsPN = Provider.of<ActionsProviderNotifier>(context);
    final categoriesPV =
        Provider.of<CategoryProviderNotifier>(context, listen: false);
    final currenciesPV = Provider.of<CurrencyList>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: BudgetPlannerText(
          text: AppLocalizations.of(context)!.all_action_screen_title,
          color: Color(0xFF0D0E56),
          fontWeight: FontWeight.bold,
          fontsize: SizeConfig.scaleTextFont(20),
        ),
        iconTheme: IconThemeData(color: Color(0xFF472FC8)),
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig.scaleHeight(34),
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
        ),
        child: Column(
          children: [
            BudgetPlannerTextField(
              hintTextField: 'Search',
              fillColor: Color(0xFFF1F4FF),
              prefixIcon: Icon(
                Icons.search,
                color: Color(0xFF0D0E56),
                size: SizeConfig.scaleWidth(40),
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(43)),
            Expanded(
              child: Consumer<ActionsProviderNotifier>(
                builder: (
                  BuildContext context,
                  ActionsProviderNotifier value,
                  Widget? child,
                ) {
                  return ListView.builder(
                    itemCount: myActionsPN.actions.length,
                    itemBuilder: (context, index) =>
                        // ActionOperation action = myActions.elementAt(index);
                        // print('My Action: 00000000000000000000000000000');
                        Column(
                      children: [
                        index != 0
                            ? Visibility(
                                visible: (myActionsPN.actions[index].date.day !=
                                    myActionsPN.actions[index - 1].date.day),
                                child: BudgetPlannerText(
                                  text: myActionsPN.actions[index].date.day ==
                                              DateTime.now().day &&
                                          myActionsPN
                                                  .actions[index].date.month ==
                                              DateTime.now().month &&
                                          myActionsPN
                                                  .actions[index].date.year ==
                                              DateTime.now().year
                                      ? 'Today'
                                      : DateFormat('d MMMM')
                                          .format(
                                              myActionsPN.actions[index].date)
                                          .toString(),
                                  fontsize: 14,
                                  color: AppColors.DATE,
                                  fontWeight: FontWeight.w600,
                                ))
                            : BudgetPlannerText(
                                text: myActionsPN.actions[index].date.day ==
                                            DateTime.now().day &&
                                        myActionsPN.actions[index].date.month ==
                                            DateTime.now().month &&
                                        myActionsPN.actions[index].date.year ==
                                            DateTime.now().year
                                    ? 'Today'
                                    : DateFormat('d MMMM')
                                        .format(myActionsPN.actions[index].date)
                                        .toString(),
                                fontsize: 14,
                                color: AppColors.DATE,
                                fontWeight: FontWeight.w600,
                              ),
                        HomeScreenListTile(
                          onTap: (){
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailsScreen(id: myActionsPN.actions[index].id,)));
                            // Navigator.push(context, MaterialPageRoute(builder: (context) => DetailsScreen(id: myActionsPN.actions[index].id)));
                          },
                          title: categoriesPV.getCategoryNameById(
                              myActionsPN.actions[index].categoryId),
                          subtitle: BudgetPlannerText(
                            text: 'Pizza Day',
                            fontsize: 15,
                            color: Color(0xFF7B7C98),
                          ),
                          trailing: amountRow(
                            myActionsPN.actions[index].amount.toString(),
                            myActionsPN.actions[index].expense!,
                            currenciesPV.getById(myActionsPN.actions[index].currencyId),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(30)),
          ],
        ),
      ),
    );
  }

  Widget amountRow(String amount, bool ex, String currency) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        BudgetPlannerText(
            text: ex ? '- ' : '+ ',
            fontsize: 20,
            fontWeight: FontWeight.w600,
            color: ex ? AppColors.AMOUNT_EXPENSE : AppColors.HOME_GREEN),
        BudgetPlannerText(
            text: currency+' ',
            fontsize: 12,
            fontWeight: FontWeight.w600,
            color: ex ? AppColors.AMOUNT_EXPENSE : AppColors.HOME_GREEN),
        BudgetPlannerText(
            text: '$amount',
            fontsize: 15,
            fontWeight: FontWeight.w600,
            color: ex ? AppColors.AMOUNT_EXPENSE : AppColors.HOME_GREEN),
      ],
    );
  }
}
