import 'dart:ui';
import 'package:budget_planner_app/models/currency.dart';
import 'package:budget_planner_app/notifiers/currencyDetails.dart';
import 'package:budget_planner_app/screens/bottom_navegationBar/add_new_operation.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CurrencySelectScreen extends StatelessWidget {

  late Currencies currency;

  @override
  Widget build(BuildContext context) {
    final currencyPv = Provider.of<CurrencyList>(context);
    return Container(
      padding: EdgeInsets.only(top:SizeConfig.scaleHeight(20)),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: BudgetPlannerText(text:'Currency',fontsize: 20,fontWeight: FontWeight.bold,textAlign: TextAlign.center,color: Color(0xFF0D0E56),),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Color(0xffF472FCB)),
          elevation: 0,
        ),
        body: Padding(
         padding:  EdgeInsets.symmetric(
         horizontal: SizeConfig.scaleWidth(20)),
          child: Container(
            margin: EdgeInsets.only(top: 10),
              child: SizedBox(
                height: SizeConfig.scaleHeight(currencyPv.cList.length*80),
                child: ListView.separated(
                  padding: EdgeInsets.all(10),
                  separatorBuilder: (ctx,index)=>Divider(),
                    itemCount: currencyPv.cList.length,
                    itemBuilder: (ctx,index) {
                      return InkWell(
                        onTap: () {
                          currencyPv.selectCurrency(
                              currencyPv.cList[index].name);
                          currency = currencyPv.cList.elementAt(index);
                          Navigator.pop(context,currency);
                        },
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: SizeConfig.scaleHeight(15)),
                              clipBehavior: Clip.antiAlias,
                              width: SizeConfig.scaleWidth(30),
                              height: SizeConfig.scaleHeight(30),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(7.5),
                              ),
                              child: Image.asset(
                                'images/${currencyPv.cList[index].name}.png',
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(width: SizeConfig.scaleWidth(10),),
                            Text(currencyPv.getFullCurrencyName(
                                currencyPv.cList[index].name)),
                            Spacer(),
                            Visibility(
                                visible: currencyPv.cList[index].isSelected,
                                child: Icon(
                                  Icons.check, color: Color(0xFF472FC8),)),
                          ],
                        ),
                      );
                    }
                ),
              ),
            ),
          ),
        ),
    //  ),
    );
  }
}
