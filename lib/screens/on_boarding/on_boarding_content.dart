import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_button.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OnBoardingContent extends StatelessWidget {
  final String textTitle;
  final String textSupTitle;
  final String pathImage;
  final String textButton;
  final double heightImage;
  final double widthImage;
  final double heightTextAndImage;
  final double heightButtonAndImage;
  final VoidCallback? onPressedPage;
  final VoidCallback? onPressedSkip;

  OnBoardingContent({
    required this.textTitle,
    required this.textSupTitle,
    required this.pathImage,
    required this.textButton,
    required this.heightTextAndImage,
    required this.heightButtonAndImage,
    required this.heightImage,
    required this.widthImage,
    required this.onPressedPage,
    required this.onPressedSkip,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: SizeConfig.scaleHeight(117)),
        BudgetPlannerText(
          text: textTitle,
          fontsize: 20,
          color: Color(0xFF0D0E56),
          textAlign: TextAlign.center,
          fontWeight: FontWeight.bold,
        ),
        SizedBox(height: SizeConfig.scaleHeight(10)),
        BudgetPlannerText(
          text: textSupTitle,
          fontsize: 15,
          color: Color(0xFF7B7C98),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: SizeConfig.scaleHeight(heightTextAndImage)),
        Image.asset( pathImage,
          height: SizeConfig.scaleHeight(heightImage),
          width: SizeConfig.scaleWidth(widthImage),
        ),
        SizedBox(height: SizeConfig.scaleHeight(heightButtonAndImage)),
        BudgetPlannerButton(onPressedPage: onPressedPage, textButton: textButton),
        SizedBox(height: SizeConfig.scaleHeight(17)),
        TextButton(
          onPressed: onPressedSkip,
          child: BudgetPlannerText(
            text: AppLocalizations.of(context)!.on_boarding_screen1_text_skip,
            fontsize: 15,
            color: Color(0xFF7B7C98),
          ),
        ),
      ],
    );
  }
}


