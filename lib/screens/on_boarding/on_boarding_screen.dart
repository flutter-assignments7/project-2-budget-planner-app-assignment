import 'package:budget_planner_app/screens/on_boarding/on_boarding_content.dart';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OnBoardingScreen extends StatefulWidget {
  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {

  late PageController _pageController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _pageController = PageController();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
        child: PageView(
          controller: _pageController,
          children: [
            OnBoardingContent(
              textTitle:
                  AppLocalizations.of(context)!.on_boarding_screen1_title,
              textSupTitle:
                  AppLocalizations.of(context)!.on_boarding_screen1_supTitle,
              pathImage: 'images/on_boarding_layer_1.png',
              textButton: AppLocalizations.of(context)!.on_boarding_screen1_text_button,
              heightTextAndImage: 146.8,
              heightButtonAndImage: 119.79,
              heightImage: 310.41,
              widthImage: 274.69,
              onPressedPage: (){
                _pageController.nextPage(duration: Duration(milliseconds: 100), curve: Curves.easeIn);
              },
              onPressedSkip: (){
                // String route = SharedPrefController().isLoggedIn() ? '/main_screen' : '/login_screen';
                SharedPrefController().preferences.setBool('ob',true);
                Navigator.pushReplacementNamed(context, '/login_screen');
              },
            ),
            OnBoardingContent(
              textTitle:
              AppLocalizations.of(context)!.on_boarding_screen2_title,
              textSupTitle:
              AppLocalizations.of(context)!.on_boarding_screen2_supTitle,
              pathImage: 'images/on_boarding_layer_2.png',
              textButton: AppLocalizations.of(context)!.on_boarding_screen1_text_button,
              heightTextAndImage: 65.5,
              heightButtonAndImage: 127.84,
              heightImage: 350.64,
              widthImage: 273.91,
              onPressedPage: (){
                _pageController.nextPage(duration: Duration(milliseconds: 100), curve: Curves.easeIn);
              },
              onPressedSkip: (){
                // String route = SharedPrefController().isLoggedIn() ? '/main_screen' : '/login_screen';
                SharedPrefController().preferences.setBool('ob',true);
                Navigator.pushReplacementNamed(context, '/login_screen');
              },
            ),
            OnBoardingContent(
              textTitle:
              AppLocalizations.of(context)!.on_boarding_screen3_title,
              textSupTitle:
              AppLocalizations.of(context)!.on_boarding_screen3_supTitle,
              pathImage: 'images/on_boarding_layer_3.png',
              textButton: AppLocalizations.of(context)!.on_boarding_screen3_text_button,
              heightTextAndImage: 111.5,
              heightButtonAndImage: 105.97,
              heightImage: 342.83,
              widthImage: 302.73,
              onPressedPage: (){
                // String route = SharedPrefController().isLoggedIn() ? '/main_screen' : '/login_screen';
                SharedPrefController().preferences.setBool('ob',true);
                Navigator.pushReplacementNamed(context, '/login_screen');
              },
              onPressedSkip: (){
                // String route = SharedPrefController().isLoggedIn() ? '/main_screen' : '/login_screen';
                SharedPrefController().preferences.setBool('ob',true);
                Navigator.pushReplacementNamed(context, '/login_screen');
              },
            ),
          ],
        ),
      ),
    );
  }
}
