import 'package:budget_planner_app/models/category.dart';
import 'package:budget_planner_app/notifiers/actions_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/category_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/currencyDetails.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_card.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/create_account_card_row.dart';
import 'package:budget_planner_app/widgets/details_screen_card_row.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

// class DetailsScreen extends StatelessWidget {
//
//   final int id;
//   DetailsScreen({
//     required this.id
//   });
//
//   @override
//   Widget build(BuildContext context) {
//
//   }
// }

class DetailsScreen extends StatefulWidget {
  final int id;

  DetailsScreen({required this.id});

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  bool? state;

  @override
  Widget build(BuildContext context) {
    final currenciesPV = Provider.of<CurrencyList>(context);
    final actionOperationPV =
        Provider.of<ActionsProviderNotifier>(context, listen: false)
            .getById(widget.id);
    final categoryPV = Provider.of<CategoryProviderNotifier>(context);

    state = actionOperationPV.expense;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            padding: EdgeInsetsDirectional.only(end: SizeConfig.scaleWidth(20)),
            constraints: BoxConstraints(),
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.close,
              color: Color(0xFF7B7C98),
            ),
          ),
        ],
        title: BudgetPlannerText(
          text: AppLocalizations.of(context)!.details_screen_title,
          color: Color(0xFF0D0E56),
          fontsize: SizeConfig.scaleTextFont(20),
          fontWeight: FontWeight.bold,
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
          top: SizeConfig.scaleHeight(15),
        ),
        child: Column(
          children: [
            BudgetPlannerCard(
              widthCard: 120,
              heightCard: 120,
              pathImage: 'images/icon_wallet.png',
              borderRadiusCard: 30,
            ),
            SizedBox(height: SizeConfig.scaleHeight(13)),
            BudgetPlannerText(
              text:
                  categoryPV.getCategoryNameById(actionOperationPV.categoryId),
              fontsize: 20,
              fontWeight: FontWeight.bold,
              color: Color(0xFF0D0E56),
            ),
            SizedBox(height: SizeConfig.scaleHeight(10)),
            Container(
              alignment: Alignment.center,
              height: SizeConfig.scaleHeight(67),
              width: double.infinity,
              color: Colors.white,
              child: BudgetPlannerText(
                text: currenciesPV.getById(actionOperationPV.currencyId) +
                    ' ' +
                    actionOperationPV.amount.toString(),
                fontsize: 21,
                fontWeight: FontWeight.w600,
                color: Color(0xFF181819),
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(11)),
            Row(
              children: [
                Expanded(
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                        width: 1,
                        color: state == true
                            ? Colors.red.shade900
                            : Colors.transparent,
                      ),
                    ),
                    child: Column(
                      children: [
                        SizedBox(height: SizeConfig.scaleHeight(23)),
                        Image.asset('images/expenses.png'),
                        SizedBox(height: SizeConfig.scaleHeight(8)),
                        BudgetPlannerText(
                          text: AppLocalizations.of(context)!
                              .add_new_category_expenses,
                          color: Color(0xFF181819),
                          fontWeight: FontWeight.w500,
                          fontsize: 15,
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: SizeConfig.scaleHeight(15)),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: SizeConfig.scaleWidth(10)),
                Expanded(
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                        width: 1,
                        color:
                            state == false ? Colors.green : Colors.transparent,
                      ),
                    ),
                    child: Column(
                      children: [
                        SizedBox(height: SizeConfig.scaleHeight(23)),
                        Image.asset('images/income.png'),
                        SizedBox(height: SizeConfig.scaleHeight(8)),
                        BudgetPlannerText(
                          text: AppLocalizations.of(context)!
                              .add_new_category_income,
                          color: Color(0xFF181819),
                          fontWeight: FontWeight.w500,
                          fontsize: 15,
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: SizeConfig.scaleHeight(15)),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: SizeConfig.scaleHeight(11)),
            Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadiusDirectional.circular(
                      SizeConfig.scaleWidth(10))),
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.scaleWidth(20),
                    vertical: SizeConfig.scaleHeight(22)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    DetailsScreenCardRow(
                      text:
                          AppLocalizations.of(context)!.details_screen_category,
                      details: categoryPV.getCategoryNameById(actionOperationPV.categoryId),
                    ),
                    Divider(
                      thickness: 1,
                      height: SizeConfig.scaleHeight(44),
                    ),
                    DetailsScreenCardRow(
                      text: AppLocalizations.of(context)!.details_screen_date,
                      details: DateFormat('E d MMMM').format(actionOperationPV.date).toString(),
                    ),
                    Divider(
                      thickness: 1,
                      height: SizeConfig.scaleHeight(44),
                    ),
                    DetailsScreenCardRow(
                      text:
                          AppLocalizations.of(context)!.details_screen_currency,
                      details: currenciesPV.getById2(actionOperationPV.currencyId),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              width: double.infinity,
              height: SizeConfig.scaleHeight(150),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadiusDirectional.circular(
                    SizeConfig.scaleWidth(10),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.scaleWidth(20),
                      vertical: SizeConfig.scaleHeight(22)),
                  child: BudgetPlannerText(
                    text:
                        actionOperationPV.notes,
                    color: Color(0xFFBABAD7),
                    fontsize: 15,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
