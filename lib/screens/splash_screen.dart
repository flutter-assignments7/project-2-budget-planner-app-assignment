import 'package:budget_planner_app/notifiers/actions_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/category_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/currencyDetails.dart';
import 'package:budget_planner_app/notifiers/user_provider_notifier.dart';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_card.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      if(SharedPrefController().preferences.containsKey('ob')){
        if(SharedPrefController().isLoggedIn()){
          Provider.of<ActionsProviderNotifier>(context,listen: false).read();
          Provider.of<UserProviderNotifier>(context,listen: false).read();
          Navigator.pushReplacementNamed(context, '/main_screen');
        }else{
          Navigator.pushReplacementNamed(context, '/login_screen');
        }
      }else{
        Navigator.pushReplacementNamed(context, '/on_boarding_screen');
      }


    });
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<CurrencyList>(context,listen: false).get();
    Provider.of<CategoryProviderNotifier>(context,listen: false).read();
    SizeConfig().init(context);
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            BudgetPlannerCard(
              widthCard: 170,
              heightCard: 170,
              borderRadiusCard: 45,
              heightImageCard: 118.4,
              widthImageCard: 86.8,
              pathImage: 'images/logo.png',
            ),
            SizedBox(height: SizeConfig.scaleHeight(30)),
            BudgetPlannerText(
              text: AppLocalizations.of(context)!.title_splash,
              textAlign: TextAlign.center,
              fontWeight: FontWeight.w800,
              color: Color(0xFF0D0E56),
              fontsize: 24,
            ),
          ],
        ),
      ),
    );
  }
}
