import 'package:budget_planner_app/notifiers/actions_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/category_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/currencyDetails.dart';
import 'package:budget_planner_app/notifiers/user_provider_notifier.dart';
import 'package:budget_planner_app/screens/details_screen.dart';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_button.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/home_screen_list_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool showLimitAlert = true;
  double totalSpent = 0.0;
  double totalBalance = 0.0;

  late double ratio ;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    final myActionsPV =
    Provider.of<ActionsProviderNotifier>(context,listen: false);
    final currenciesPV = Provider.of<CurrencyList>(context, listen: false);
    final categoriesPV =
    Provider.of<CategoryProviderNotifier>(context, listen: false);

    myActionsPV.todayOperations.forEach((element) {
      if (element.expense!) {
        totalSpent += currenciesPV.convertOperations(SharedPrefController().preferences.getInt('currency_id')!, element);
      } else {
        totalBalance +=
            currenciesPV.convertOperations(SharedPrefController().preferences.getInt('currency_id')!, element);
      }
    });
    ratio = (totalSpent * 100 / totalBalance) * 0.01;
  }

  @override
  Widget build(BuildContext context) {
    // final user = Provider.of<UserProviderNotifier>(context,listen: false);

    return Padding(
      padding: EdgeInsetsDirectional.only(
        start: SizeConfig.scaleWidth(20),
        end: SizeConfig.scaleWidth(20),
      ),
      child: Column(
        children: [
          SizedBox(height: SizeConfig.scaleHeight(2)),
          Visibility(
            visible: totalSpent >= SharedPrefController().preferences.getDouble('day_limit')!,
            child: Visibility(
              visible: showLimitAlert,
              child: Card(
                elevation: 4,
                margin: EdgeInsets.zero,
                child: Row(
                  children: [
                    SizedBox(
                      width: SizeConfig.scaleWidth(10),
                    ),
                    BudgetPlannerText(
                      text: '!',
                      fontsize: 30,
                      color: AppColors.AMOUNT_EXPENSE,
                      fontWeight: FontWeight.bold,
                    ),
                    Spacer(),
                    BudgetPlannerText(
                      text: 'Your daily limit has been reached',
                      fontsize: 15,
                      color: AppColors.TITLE,
                      fontWeight: FontWeight.w500,
                    ),
                    Spacer(),
                    IconButton(
                      onPressed: () {
                        setState(() {
                          showLimitAlert = false;
                        });
                      },
                      icon: Icon(
                        Icons.cancel,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Card(
            elevation: 7,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(130),
              // side: BorderSide(
              //   color: Colors.transparent,
              //   width: 1,
              // ),
            ),
            child: CircularPercentIndicator(
              radius: 250,
              lineWidth: 10,
              circularStrokeCap: CircularStrokeCap.round,
              curve: Curves.easeIn,
              animationDuration: 1000,
              animateFromLastPercent: true,
              startAngle: 0,
              animation: true,
              progressColor: ratio >= 0.9
                  ? Colors.red
                  : ratio <= 0.5
                      ? AppColors.HOME_GREEN
                      : AppColors.PROGRESS_BAR_FILL,
              backgroundColor: Colors.transparent,
              percent: ratio > 1 ? 1 : ratio,
              reverse: true,
              center: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        '${Provider.of<CurrencyList>(context).getById(SharedPrefController().preferences.getInt('currency_id')!)}',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: SizeConfig.scaleTextFont(18.5),
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Montserrat'),
                      ),
                      BudgetPlannerText(
                        text: '$totalSpent',
                        fontsize: SizeConfig.scaleTextFont(40),
                        fontWeight: FontWeight.w600,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  BudgetPlannerText(
                    text: AppLocalizations.of(context)!.home_screen_spent_today,
                    color: Color(0xFF7B7C98),
                    fontsize: SizeConfig.scaleTextFont(16.5),
                  ),
                  Divider(
                    thickness: 1,
                    indent: SizeConfig.scaleWidth(33.55),
                    endIndent: SizeConfig.scaleWidth(33.55),
                    height: 30,
                  ),
                  BudgetPlannerText(
                    text: AppLocalizations.of(context)!
                        .home_screen_balance_for_today,
                    color: Color(0xFF7B7C98),
                    fontsize: SizeConfig.scaleTextFont(16.5),
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(6)),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        '${Provider.of<CurrencyList>(context).getById(SharedPrefController().preferences.getInt('currency_id')!)}',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: SizeConfig.scaleTextFont(13),
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Montserrat',
                          color: Color(0xFF00BEA1),
                        ),
                      ),
                      BudgetPlannerText(
                        text: '$totalBalance',
                        fontsize: SizeConfig.scaleTextFont(23),
                        fontWeight: FontWeight.w600,
                        textAlign: TextAlign.start,
                        color: Color(0xFF00BEA1),
                      ),
                    ],
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(10)),
                ],
              ),
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(23)),
          Row(
            children: [
              BudgetPlannerText(
                text: AppLocalizations.of(context)!.home_screen_last_actions,
                fontsize: SizeConfig.scaleTextFont(20),
                fontWeight: FontWeight.bold,
                textAlign: TextAlign.center,
                color: Color(0xFF0D0E56),
              ),
            ],
          ),
          SizedBox(height: SizeConfig.scaleHeight(10)),
          Expanded(
            child: Card(
              elevation: 4,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.zero,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleHeight(12)),
                child: Column(
                  children: [
                    Expanded(
                      child: Consumer(
                        builder: (
                          BuildContext context,
                          ActionsProviderNotifier value,
                          Widget? child,
                        ) {
                          return ListView.builder(
                            itemCount: value.actions.length < 4
                                ? value.actions.length
                                : 4,
                            itemBuilder: (context, index) => Column(
                              children: [
                                index != 0
                                    ? Visibility(
                                        visible: (value
                                                .actions[index].date.day !=
                                            value.actions[index - 1].date.day),
                                        child: BudgetPlannerText(
                                          text: value.actions[index].date.day ==
                                                      DateTime.now().day &&
                                                  value.actions[index].date
                                                          .month ==
                                                      DateTime.now().month &&
                                                  value.actions[index].date.year ==
                                                      DateTime.now().year
                                              ? 'Today'
                                              : DateFormat('d MMMM')
                                                  .format(
                                                      value.actions[index].date)
                                                  .toString(),
                                          fontsize: 14,
                                          color: AppColors.DATE,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      )
                                    : BudgetPlannerText(
                                        text: value.actions[index].date.day ==
                                                    DateTime.now().day &&
                                                value.actions[index].date.month ==
                                                    DateTime.now().month &&
                                                value.actions[index].date.year ==
                                                    DateTime.now().year
                                            ? 'Today'
                                            : DateFormat('d MMMM')
                                                .format(
                                                    value.actions[index].date)
                                                .toString(),
                                        fontsize: 14,
                                        color: AppColors.DATE,
                                        fontWeight: FontWeight.w600,
                                      ),
                                HomeScreenListTile(
                                  title: Provider.of<CategoryProviderNotifier>(context).getCategoryNameById(
                                      value.actions[index].categoryId),
                                  subtitle: BudgetPlannerText(
                                    text: 'Pizza Day',
                                    fontsize: 15,
                                    color: Color(0xFF7B7C98),
                                  ),
                                  trailing: amountRow(
                                    value.actions[index].amount.toString(),
                                    value.actions[index].expense!,
                                    Provider.of<CurrencyList>(context).getById(
                                        value.actions[index].currencyId),
                                  ),
                                  onTap: (){
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailsScreen(id: value.actions[index].id,)));
                                    // Navigator.push(context, MaterialPageRoute(builder: (context) => DetailsScreen(id: myActionsPV.actions[index].id)));
                                  },
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(15)),
                    Padding(
                      padding: EdgeInsetsDirectional.only(
                        start: SizeConfig.scaleWidth(4),
                        end: SizeConfig.scaleWidth(4),
                      ),
                      child: BudgetPlannerButton(
                        onPressedPage: () {
                          Navigator.pushNamed(context, '/all_actions_screen');
                        },
                        textButton:
                        AppLocalizations.of(context)!.home_screen_btn_text,
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(15)),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(10)),
        ],
      ),
    );
  }
}

Widget amountRow(String amount, bool ex, String currency) {
  return Row(
    mainAxisSize: MainAxisSize.min,
    children: [
      BudgetPlannerText(
          text: ex ? '- ' : '+ ',
          fontsize: 20,
          fontWeight: FontWeight.w600,
          color: ex ? AppColors.AMOUNT_EXPENSE : AppColors.HOME_GREEN),
      BudgetPlannerText(
          text: currency + ' ',
          fontsize: 12,
          fontWeight: FontWeight.w600,
          color: ex ? AppColors.AMOUNT_EXPENSE : AppColors.HOME_GREEN),
      BudgetPlannerText(
          text: '$amount',
          fontsize: 15,
          fontWeight: FontWeight.w600,
          color: ex ? AppColors.AMOUNT_EXPENSE : AppColors.HOME_GREEN),
    ],
  );
}

// Card(
//   elevation: 4,
//   shape: RoundedRectangleBorder(
//     borderRadius: BorderRadius.circular(10),
//   ),
//   child: Padding(
//     padding: EdgeInsetsDirectional.only(
//       start: SizeConfig.scaleWidth(12),
//       end: SizeConfig.scaleWidth(12),
//       top: SizeConfig.scaleHeight(20),
//       bottom: SizeConfig.scaleHeight(25),
//     ),
//      child: Column(
//        children: [
//          Expanded(
//            child: ListView.builder(
//               itemCount: myActionsPV.actions.length < 4 ? myActionsPV.actions.length : 4,
//               itemBuilder: (context, index) =>
//               // ActionOperation action = myActions.elementAt(index);
//               // print('My Action: 00000000000000000000000000000');
//               Column(
//                 children: [
//                   index != 0
//                       ? Visibility(
//                       visible: (myActionsPV.actions[index].date.day !=
//                           myActionsPV.actions[index - 1].date.day),
//                       child: BudgetPlannerText(
//                         text: myActionsPV.actions[index].date.day ==
//                             DateTime.now().day &&
//                             myActionsPV
//                                 .actions[index].date.month ==
//                                 DateTime.now().month &&
//                             myActionsPV
//                                 .actions[index].date.year ==
//                                 DateTime.now().year
//                             ? 'Today'
//                             : DateFormat('d MMMM')
//                             .format(
//                             myActionsPV.actions[index].date)
//                             .toString(),
//                         fontsize: 14,
//                         color: AppColors.DATE,
//                         fontWeight: FontWeight.w600,
//                       ))
//                       : BudgetPlannerText(
//                     text: myActionsPV.actions[index].date.day ==
//                         DateTime.now().day &&
//                         myActionsPV.actions[index].date.month ==
//                             DateTime.now().month &&
//                         myActionsPV.actions[index].date.year ==
//                             DateTime.now().year
//                         ? 'Today'
//                         : DateFormat('d MMMM')
//                         .format(myActionsPV.actions[index].date)
//                         .toString(),
//                     fontsize: 14,
//                     color: AppColors.DATE,
//                     fontWeight: FontWeight.w600,
//                   ),
//                   HomeScreenListTile(
//                     title: categoriesPV.getCategoryNameById(
//                         myActionsPV.actions[index].categoryId),
//                     subtitle: BudgetPlannerText(
//                       text: 'Pizza Day',
//                       fontsize: 15,
//                       color: Color(0xFF7B7C98),
//                     ),
//                     trailing: amountRow(
//                       myActionsPV.actions[index].amount.toString(),
//                       myActionsPV.actions[index].expense!,
//                       currenciesPV.getById(myActionsPV.actions[index].currencyId),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//          ),
//        ],
//      )
//
//   ),
// ),

// child: Column(
// children: [
// BudgetPlannerText(
// text: 'Today',
// fontsize: SizeConfig.scaleTextFont(12),
// fontWeight: FontWeight.w600,
// color: Color(0xFFB9BACE),
// ),
// HomeScreenListTile(
// title: 'Food',
// onTap: (){
// Navigator.pushNamed(context, '/details_screen');
// },
// subtitle: BudgetPlannerText(
// text: 'Pizza Day',
// fontsize: 15,
// color: Color(0xFF7B7C98),
// ),
// trailing: BudgetPlannerText(
// text: '- \$223',
// fontsize: 15,
// fontWeight: FontWeight.w600,
// color: Color(0xFFF33720),
// ),
// ),
// Divider(
// thickness: 0.3,
// indent: SizeConfig.scaleWidth(12),
// endIndent: SizeConfig.scaleWidth(12),
// height: 10,
// ),
// HomeScreenListTile(
// title: 'Food',
// subtitle: BudgetPlannerText(
// text: 'Pizza Day',
// fontsize: 15,
// color: Color(0xFF7B7C98),
// ),
// trailing: BudgetPlannerText(
// text: '- \$223',
// fontsize: 15,
// fontWeight: FontWeight.w600,
// color: Color(0xFFF33720),
// ),
// ),
// SizedBox(height: SizeConfig.scaleHeight(25)),
// Padding(
// padding: EdgeInsetsDirectional.only(
// start: SizeConfig.scaleWidth(4),
// end: SizeConfig.scaleWidth(4),
// ),
// child: BudgetPlannerButton(
// onPressedPage: () {
// Navigator.pushNamed(context, '/all_actions_screen');
// },
// textButton:
// AppLocalizations.of(context)!.home_screen_btn_text,
// ),
// ),
// ],
// ),
