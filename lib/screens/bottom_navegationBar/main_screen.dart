import 'package:budget_planner_app/models/bn_screen.dart';
import 'package:budget_planner_app/screens/bottom_navegationBar/category_screen.dart';
import 'package:budget_planner_app/screens/bottom_navegationBar/home_screen.dart';
import 'package:budget_planner_app/screens/bottom_navegationBar/profile_screen.dart';
import 'package:budget_planner_app/screens/bottom_navegationBar/tips_screen.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _currentIndex = 0;

  // late BuildContext _context;

  List<BottomNavigationScreen> _bnScreen = <BottomNavigationScreen>[
    BottomNavigationScreen(
      'Home',
      HomeScreen(),
      Icons.settings,
    ),
    BottomNavigationScreen(
      'Category',
      CategoriesScreen(),
      Icons.add_circle,
    ),
    BottomNavigationScreen(
      'ssss',
      CategoriesScreen(),
      Icons.settings,
    ),
    BottomNavigationScreen(
      'Profile',
      ProfileScreen(),
      Icons.settings,
    ),
    BottomNavigationScreen(
      'Tips',
      TipsScreen(),
      Icons.settings,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    // _context = context;
    SizeConfig().init(context);
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: BudgetPlannerText(
            text: _bnScreen.elementAt(_currentIndex).title,
            fontsize: SizeConfig.scaleTextFont(20),
            fontWeight: FontWeight.bold,
            color: Color(0xFF0D0E56),
            textAlign: TextAlign.center,
          ),
          actions: [
            if(_currentIndex == 0 || _currentIndex == 1)
            IconButton(
              onPressed: (){
                if(_currentIndex == 0){
                  Navigator.pushNamed(context, '/settings_screen');
                }else if(_currentIndex == 1){
                  Navigator.pushNamed(context, '/add_new_category');
                }
              },
              icon: Icon(
                _bnScreen.elementAt(_currentIndex).iconData,
                color: Color(0xFF472FC8),
              ),
            )
          ],
        ),
        body: _bnScreen.elementAt(_currentIndex).widget,

        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          elevation: 0,
          backgroundColor: Color(0xFF979797).withOpacity(0.1),
          selectedItemColor: Color(0xFF0D0E56),
          unselectedItemColor: Color(0xFFD3CFEA),
          currentIndex: _currentIndex,
          onTap: (int selectedIndex) {
            setState(() {
              _currentIndex = selectedIndex;
            });
          },
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Image.asset(
                'images/ic-Dashboard.png',
                color: _currentIndex == 0 ? Color(0xFF0D0E56) : Color(0xFFD3CFEA),
              ),
              label: AppLocalizations.of(context)!.main_screen_home,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.category),
              label: AppLocalizations.of(context)!.main_screen_category,
            ),
            BottomNavigationBarItem(
              icon: FloatingActionButton(
                elevation: 0,
                backgroundColor: Color(0xFF472FC8),
                onPressed: () {
                  Navigator.pushNamed(context, '/add_new_operation');
                },
                child: Icon(
                  Icons.add,
                  size: SizeConfig.scaleWidth(35),
                ),
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: AppLocalizations.of(context)!.main_screen_profile,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.error_outline),
              label: AppLocalizations.of(context)!.main_screen_tips,
            ),
          ],
        ),
      ),
    );
  }
}


