import 'package:budget_planner_app/models/contact.dart';
import 'package:budget_planner_app/storage/tips.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/material.dart';

class TipsDetailsScreen extends StatefulWidget {
  const TipsDetailsScreen({Key? key}) : super(key: key);

  @override
  _TipsDetailsScreenState createState() => _TipsDetailsScreenState();
}

class _TipsDetailsScreenState extends State<TipsDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // backwardsCompatibility: true,
        // excludeHeaderSemantics: true,
        // primary: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(
          color: Color(0xFF472FC8),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.ios_share,
              color: Color(0xFF472FC8),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
          bottom: SizeConfig.scaleHeight(10),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              'images/on_boarding_layer_1.png',
              width: double.infinity,
              height: SizeConfig.scaleHeight(414),
            ),
            BudgetPlannerText(
              text: Tip.tips[0].title,
              fontsize: 20,
              fontWeight: FontWeight.bold,
              color: AppColors.TITLE,
              textAlign: TextAlign.start,
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            BudgetPlannerText(
              text: Tip.tips[0].body,
              fontsize: 15,
              color: AppColors.SUBTITLE,
              textAlign: TextAlign.start,
            ),
          ],
        ),
      ),
    );
  }
}
