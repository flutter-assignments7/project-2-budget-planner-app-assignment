import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/card_tip.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class TipsScreen extends StatelessWidget {
  const TipsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.symmetric(
        horizontal: SizeConfig.scaleWidth(20),
      ),
      child: Column(
        children: [
          TextField(

          ),
          Expanded(
            child: GridView.builder(
              itemCount: 6,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 3/2,
                  mainAxisExtent: 180,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10
              ),
              itemBuilder: (ctx,index){
                return CardTip(
                  onTap: (){
                    Navigator.pushNamed(context, '/tips_details_screen');
                  },
                );
              },
            ),
          ),
        ],
      ),
    );

  }
}
