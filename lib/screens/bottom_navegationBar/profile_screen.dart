 import 'package:budget_planner_app/notifiers/currencyDetails.dart';
import 'package:budget_planner_app/notifiers/user_provider_notifier.dart';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_button.dart';
import 'package:budget_planner_app/widgets/budget_planner_card.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/create_account_card_row.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserProviderNotifier>(context);
    final currenciesPV = Provider.of<CurrencyList>(context);
    
    return Padding(
      padding: EdgeInsetsDirectional.only(
        start: SizeConfig.scaleWidth(20),
        end: SizeConfig.scaleWidth(20),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: [
            BudgetPlannerCard(
              widthCard: 120,
              heightCard: 120,
              pathImage: 'images/ic-profile-inactive.png',
              borderRadiusCard: 30,
              widthImageCard: 56.81,
              heightImageCard: 52.93,
            ),
            SizedBox(height: SizeConfig.scaleHeight(13)),
            BudgetPlannerText(
              text: SharedPrefController().preferences.getString('name')!,
              color: Color(0xFF0D0E56),
              fontsize: 20,
              textAlign: TextAlign.center,
              fontWeight: FontWeight.bold,
            ),
            SizedBox(height: SizeConfig.scaleHeight(21)),
            Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadiusDirectional.circular(
                      SizeConfig.scaleWidth(10))),
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.scaleWidth(15),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CreateAccountCardRow(
                      readOnly: true,
                      title: AppLocalizations.of(context)!
                          .create_account_text_field_name,
                      hintTextField: SharedPrefController().preferences.getString('name')!,
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    CreateAccountCardRow(
                      readOnly: true,
                      textInputType: TextInputType.emailAddress,
                      title: AppLocalizations.of(context)!
                          .create_account_text_field_email_address,
                      hintTextField: SharedPrefController().preferences.getString('email')!,
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(20)),
                    Row(
                      children: [
                        BudgetPlannerText(
                          text: AppLocalizations.of(context)!
                              .create_account_text_field_currency,
                          fontWeight: FontWeight.w500,
                          fontsize: SizeConfig.scaleTextFont(15),
                          color: Color(0xFF181819),
                        ),
                        Spacer(),
                        BudgetPlannerText(
                          text: currenciesPV.getById2(
                              SharedPrefController().preferences.getInt('currency_id')!),
                          fontsize: SizeConfig.scaleTextFont(15),
                          color: Color(0xFF7B7C98),
                        ),
                        SizedBox(width: SizeConfig.scaleWidth(2)),
                        IconButton(
                          padding: EdgeInsetsDirectional.all(0),
                          iconSize: SizeConfig.scaleWidth(15),
                          alignment: Alignment.center,
                          constraints: BoxConstraints(),
                          onPressed: () {},
                          icon: Icon(
                            Icons.arrow_forward_ios,
                            color: Color(0xFF555568),
                            size: SizeConfig.scaleWidth(17),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(20)),
                    Divider(
                      thickness: 1,
                    ),
                    CreateAccountCardRow(
                      readOnly: true,
                      textInputType: TextInputType.number,
                      title: AppLocalizations.of(context)!
                          .create_account_text_field_daily_limit,
                      hintTextField: SharedPrefController().preferences.getDouble('day_limit')!.toString(),
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(20)),
                    GestureDetector(
                      onTap: (){
                        // Navigator.pushNamed(context, '/pin_code_screen');
                      },

                      child: Container(
                        width: double.infinity,
                        child: BudgetPlannerText(
                          text: AppLocalizations.of(context)!
                              .profile_screen_change_pin,
                          fontWeight: FontWeight.w500,
                          fontsize: SizeConfig.scaleTextFont(15),
                          color: Color(0xFF181819),
                        ),
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(20)),
                  ],
                ),
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(30)),
            BudgetPlannerButton(
              onPressedPage: (){},
              textButton: AppLocalizations.of(context)!.profile_screen_btn_text,
            ),
          ],
        ),
      ),
    );
  }
}
