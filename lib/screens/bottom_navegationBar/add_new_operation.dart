import 'package:budget_planner_app/models/actions.dart';
import 'package:budget_planner_app/models/category.dart';
import 'package:budget_planner_app/models/currency.dart';
import 'package:budget_planner_app/notifiers/actions_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/category_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/currencyDetails.dart';
import 'package:budget_planner_app/screens/categories_screen2.dart';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/helpers.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_button.dart';
import 'package:budget_planner_app/widgets/budget_planner_card.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/date_widget.dart';
import 'package:budget_planner_app/widgets/details_screen_card_row.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class AddNewOperation extends StatefulWidget {
  // AddNewOperation.fromCategory(this.category);
  // AddNewOperation.fromCurrency(this.currency);

  @override
  _AddNewOperationState createState() => _AddNewOperationState();
}

class _AddNewOperationState extends State<AddNewOperation> with Helpers{
  Category? _category;
  Currencies? currency;

  bool? state;

  DateTime date = DateTime.now();
  late DateTime? picked;
  late final String hintTextDate;

  // final _formKey = GlobalKey<FormState>();
  late TextEditingController _noteTextController;
  late TextEditingController _amountTextController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _amountTextController = TextEditingController();
    _noteTextController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _amountTextController.dispose();
    _noteTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            padding: EdgeInsetsDirectional.only(end: SizeConfig.scaleWidth(20)),
            constraints: BoxConstraints(),
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.close,
              color: Color(0xFF7B7C98),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsetsDirectional.only(
            start: SizeConfig.scaleWidth(20),
            end: SizeConfig.scaleWidth(20),
            top: SizeConfig.scaleHeight(15),
          ),
          child: Column(
            children: [
              BudgetPlannerCard(
                widthCard: 120,
                heightCard: 120,
                pathImage: 'images/icon_wallet.png',
                borderRadiusCard: 30,
              ),
              SizedBox(height: SizeConfig.scaleHeight(13)),
              BudgetPlannerText(
                text: AppLocalizations.of(context)!.add_new_operation_title,
                fontsize: 20,
                fontWeight: FontWeight.bold,
                color: Color(0xFF0D0E56),
              ),
              SizedBox(height: SizeConfig.scaleHeight(10)),
              Container(
                alignment: Alignment.center,
                height: SizeConfig.scaleHeight(67),
                width: double.infinity,
                color: Colors.white,
                child: TextField(
                    controller: _amountTextController,
                    cursorColor: Colors.black,
                    cursorWidth: 1,
                    cursorHeight: 20,
                    maxLines: 1,
                    style: TextStyle(
                      fontSize: SizeConfig.scaleTextFont(21),
                      fontFamily: 'Montserrat',
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                    ),
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      filled: true,
                      contentPadding: EdgeInsetsDirectional.only(
                        start: SizeConfig.scaleWidth(15),
                        top: SizeConfig.scaleHeight(15),
                        bottom: SizeConfig.scaleHeight(15),
                        end: SizeConfig.scaleWidth(15),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.transparent,
                          width: SizeConfig.scaleWidth(1),
                        ),
                        borderRadius: BorderRadius.circular(
                          SizeConfig.scaleWidth(11),
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(
                          SizeConfig.scaleWidth(11),
                        ),
                      ),
                      hintText: '\$ 0,00',
                      hintStyle: TextStyle(
                        fontSize: 21,
                        fontFamily: 'Montserrat',
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    )),
              ),
              SizedBox(height: SizeConfig.scaleHeight(11)),
              Row(
                children: [
                  Expanded(
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(
                          width: 1,
                          color: state == true
                              ? Colors.red.shade900
                              : Colors.transparent,
                        ),
                      ),
                      child: Column(
                        children: [
                          SizedBox(height: SizeConfig.scaleHeight(23)),
                          Image.asset('images/expenses.png'),
                          SizedBox(height: SizeConfig.scaleHeight(8)),
                          BudgetPlannerText(
                            text: AppLocalizations.of(context)!
                                .add_new_category_expenses,
                            color: Color(0xFF181819),
                            fontWeight: FontWeight.w500,
                            fontsize: 15,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: SizeConfig.scaleHeight(15)),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(width: SizeConfig.scaleWidth(10)),
                  Expanded(
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(
                          width: 1,
                          color: state == false
                              ? Colors.green
                              : Colors.transparent,
                        ),
                      ),
                      child: Column(
                        children: [
                          SizedBox(height: SizeConfig.scaleHeight(23)),
                          Image.asset('images/income.png'),
                          SizedBox(height: SizeConfig.scaleHeight(8)),
                          BudgetPlannerText(
                            text: AppLocalizations.of(context)!
                                .add_new_category_income,
                            color: Color(0xFF181819),
                            fontWeight: FontWeight.w500,
                            fontsize: 15,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: SizeConfig.scaleHeight(15)),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: SizeConfig.scaleHeight(11)),
              Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(
                        SizeConfig.scaleWidth(10))),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.scaleWidth(20),
                      vertical: SizeConfig.scaleHeight(22)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () async {
                          var result = await Navigator.pushNamed(
                              context, '/categories_screen_2');
                          // print(result.name);
                          setState(() {
                            _category = result as Category?;
                            print(_category!.name);
                            state = _category!.expense;
                          });
                        },
                        child: DetailsScreenCardRow(
                          text: AppLocalizations.of(context)!
                              .details_screen_category,
                          details: _category?.name ?? '000',
                          // _category?.name ?? '',
                        ),
                      ),
                      Divider(
                        thickness: 1,
                        height: SizeConfig.scaleHeight(44),
                      ),
                      InkWell(
                        onTap: () async {
                          picked = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(2019),
                            lastDate: DateTime(2022),
                          );
                          if (picked != null && picked != date) {
                            setState(() {
                              date = picked!;
                            });
                          }
                        },
                        child: DetailsScreenCardRow(
                          text:
                              AppLocalizations.of(context)!.details_screen_date,
                          details: DateFormat('E d MMMM').format(date),
                        ),
                      ),
                      Divider(
                        thickness: 1,
                        height: SizeConfig.scaleHeight(44),
                      ),
                      InkWell(
                        onTap: () async {
                          var resultCurrency = await Navigator.pushNamed(
                              context, '/currency_select_screen');
                          setState(() {
                            currency = resultCurrency as Currencies?;
                            print(currency);
                          });
                        },
                        child: DetailsScreenCardRow(
                          text: AppLocalizations.of(context)!
                              .details_screen_currency,
                          details: currency?.name ?? '000',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: SizeConfig.scaleHeight(112),
                child: Card(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: TextField(
                    cursorColor: Colors.black,
                    cursorWidth: 1,
                    cursorHeight: 20,
                    maxLines: 3,
                    style: TextStyle(
                      fontSize: SizeConfig.scaleTextFont(20),
                    ),
                    keyboardType: TextInputType.text,
                    controller: _noteTextController,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      filled: true,
                      contentPadding: EdgeInsetsDirectional.only(
                        start: SizeConfig.scaleWidth(15),
                        top: SizeConfig.scaleHeight(15),
                        bottom: SizeConfig.scaleHeight(15),
                        end: SizeConfig.scaleWidth(15),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.transparent,
                          width: SizeConfig.scaleWidth(1),
                        ),
                        borderRadius: BorderRadius.circular(
                          SizeConfig.scaleWidth(11),
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(
                          SizeConfig.scaleWidth(11),
                        ),
                      ),
                      hintText: 'Note',
                      hintStyle: TextStyle(
                        fontSize: 15,
                        fontFamily: 'Montserrat',
                        color: Color(0xFFBABAD7),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(30)),
              BudgetPlannerButton(
                onPressedPage: () {
                  performSave();
                  // print('Iam In Save' + save().toString());
                },
                textButton:
                    AppLocalizations.of(context)!.add_new_operation_btn_text,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future performSave() async {
    if (checkData()) save();
  }

  bool checkData() {
    if (_amountTextController.text.isNotEmpty &&
        _noteTextController.text.isNotEmpty &&
        state != null &&
        _category != null &&
        currency != null) {
      Navigator.pushReplacementNamed(context, '/new_operation_success');
      return true;
    }
    showSnackBar(context, message: 'Enter required data!', error: true);
    return false;
  }

  Future save() async {
    Provider.of<ActionsProviderNotifier>(context, listen: false)
        .create(actions);
    clear();
  }

  ActionOperation get actions {
    return ActionOperation(
      amount: int.parse(_amountTextController.text),
      expense: state,
      notes: _noteTextController.text,
      categoryId: _category!.id,
      currencyId: currency!.id,
      date: date,
      userId: SharedPrefController().id,
    );
  }

  void clear() {
    _amountTextController.text = '';
    _noteTextController.text = '';
    state = null;
  }

  String get _formatedDate {
    DateTime date = DateTime.now();
    return DateFormat('E d MMMM').format(date);
  }
}
