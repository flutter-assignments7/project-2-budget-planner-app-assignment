import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/material.dart';

class ProfileScreenCard extends StatelessWidget {

  final String title;
  final Widget? leading;
  final Widget? trailing;
  final GestureTapCallback? onTap;
  final Color borderColor;
  final double borderWidth;
  final Color cardColor;
  final Color titleColor;


  ProfileScreenCard({
    required this.title,
    this.leading,
    this.trailing,
    this.onTap,
    this.borderColor = Colors.transparent,
    this.borderWidth = 0,
    this.cardColor = Colors.white,
    this.titleColor = Colors.black,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      color: cardColor,
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(10)),
        side: BorderSide(
          color: borderColor,
          width: borderWidth
        ),
      ),
      child: ListTile(
        horizontalTitleGap: 0,
        onTap: onTap,
        contentPadding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
          top: SizeConfig.scaleHeight(10),
          bottom: SizeConfig.scaleHeight(10),
        ),
        title: BudgetPlannerText(
          text: title,
          textAlign: TextAlign.start,
          fontsize: 15,
          fontWeight: FontWeight.w500,
          color: titleColor,
        ),
        leading: leading,
        trailing: trailing,

      ),
    );
  }
}