import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'budget_planner_text.dart';

class PinCodeCardNumber extends StatelessWidget {
  final String num;

  PinCodeCardNumber({
    required this.num,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: SizeConfig.scaleHeight(71),
      width: SizeConfig.scaleWidth(71),
      child: Card(
        clipBehavior: Clip.antiAlias,
        margin: EdgeInsetsDirectional.only(
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(19)),
        ),
        elevation: 7,
        child: TextButton(
          onPressed: (){},
          child: BudgetPlannerText(
            text: num,
            color: Color(0xFF472FC8),
            fontsize: 23,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
