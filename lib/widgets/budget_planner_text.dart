import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';

class BudgetPlannerText extends StatelessWidget {
  final String text;
  final Color color;
  final String fontFamily;
  final double fontsize;
  final FontWeight fontWeight;
  final TextAlign textAlign;

  BudgetPlannerText({
    required this.text,
    this.color = Colors.black,
    this.fontFamily = 'Montserrat',
    this.fontsize = 16,
    this.fontWeight = FontWeight.normal,
    this.textAlign = TextAlign.start,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      // AppLocalizations.of(context)!.title_splash,
      style: TextStyle(
        color: color, //Color(0xFF0D0E56)
        fontFamily: fontFamily,
        fontSize: SizeConfig.scaleTextFont(fontsize),
        fontWeight: fontWeight,
      ),
      textAlign: textAlign,
    );
  }
}
