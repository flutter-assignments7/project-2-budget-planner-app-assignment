import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class PinContainer extends StatelessWidget {
final String passCode;
final VoidCallback backSpaceFunction;
PinContainer({
  required this.passCode,
  required this.backSpaceFunction,
});
  @override
  Widget build(BuildContext context) {

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
            SizedBox(
                width: SizeConfig.scaleWidth(70),
                height: SizeConfig.scaleHeight(50),
                child: IconButton(
                  icon: Icon(Icons.backspace,size: 30,),
                  color: Color(0xFF472FC8),
                  onPressed: passCode.length > 0
                      ? backSpaceFunction
                      : null,
                )),

        numberContainer(t: passCode.length>=1?'${passCode[0]}':''),
        numberContainer(t: passCode.length>=2?'${passCode[1]}':''),
        numberContainer(t: passCode.length>=3?'${passCode[2]}':''),
        numberContainer(t: passCode.length==4?'${passCode[3]}':''),
        SizedBox(
          width: SizeConfig.scaleWidth(70),
          height: SizeConfig.scaleHeight(50),
        )
      ],
    );
  }
}

// ignore: camel_case_types
class numberContainer extends StatelessWidget {
  const numberContainer({
    Key? key,
    required this.t,
  }) : super(key: key);

  final String t;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(5)),
      alignment: Alignment.center,
      width: SizeConfig.scaleWidth(45),
      height: SizeConfig.scaleHeight(45),
      decoration: BoxDecoration(
        color: t.isNotEmpty?Color(0xffF472FCB):Colors.white,
        borderRadius: BorderRadius.circular(9),
        boxShadow: [
          BoxShadow(color: Colors.grey.withOpacity(0.5),blurRadius: 3),
        ],
      ),
      child: BudgetPlannerText(text:t,fontsize: 21,fontWeight: FontWeight.bold,color: Colors.white,),
    );
  }
}
