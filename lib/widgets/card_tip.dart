import 'package:budget_planner_app/storage/tips.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CardTip extends StatelessWidget {
  final GestureTapCallback? onTap;

  CardTip({
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          color: Colors.teal,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          children: [
            Image.asset(
              'images/on_boarding_layer_1.png',
              width: SizeConfig.scaleWidth(120),
              height: SizeConfig.scaleHeight(120),
              fit: BoxFit.fill,
            ),
            BudgetPlannerText(
                text: Tip.tips[0].title,
                fontsize: 15,
                fontWeight: FontWeight.w600,
                color: AppColors.TITLE,
                textAlign: TextAlign.center),
            BudgetPlannerText(
                text: Tip.tips[0].date,
                fontsize: 13,
                fontWeight: FontWeight.w600,
                color: AppColors.SUBTITLE,
                textAlign: TextAlign.center),
          ],
        ),
      ),
    );
  }
}
