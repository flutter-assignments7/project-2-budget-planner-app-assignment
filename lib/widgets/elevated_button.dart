// import 'dart:ui';
//
// import 'package:budget_planner/utils/app_colors.dart';
// import 'package:budget_planner/utils/size_config.dart';
// import 'package:budget_planner/widgets/my_text.dart';
// import 'package:flutter/material.dart';
//
// class MyElevatedButton extends StatelessWidget {
//   final VoidCallback? onPressed;
//   final String buttonTitle;
//    double borderRadius;
//   final double width;
//   final double height;
//    Color color;
//
//    MyElevatedButton({
//     required this.buttonTitle,
//       this.onPressed,
//      required this.height,
//      required this.width,
//      this.borderRadius = 30,
//      this.color = AppColors.PROGRESS_BAR_FILL,
// });
//
//   @override
//   Widget build(BuildContext context) {
//     return ElevatedButton(
//         onPressed: onPressed,
//         child: MyText(
//           text: buttonTitle,
//           fontSize: 15,
//           fontWeight: FontWeight.bold,
//           textAlign: TextAlign.center,
//           color: Colors.white,
//         ),
//       style: ElevatedButton.styleFrom(
//         primary: color,
//         shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
//         minimumSize: Size(SizeConfig.scaleWidth(width),SizeConfig.scaleHeight(height)),
//       ),
//     );
//   }
// }
