// import 'package:budget_planner_app/utils/size_config.dart';
// import 'package:budget_planner_app/widgets/budget_planner_button.dart';
// import 'package:budget_planner_app/widgets/budget_planner_text.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_gen/gen_l10n/app_localizations.dart';
//
// class DateWidget extends StatefulWidget {
//   const DateWidget({Key? key}) : super(key: key);
//
//   @override
//   _DateWidgetState createState() => _DateWidgetState();
// }
//
// class _DateWidgetState extends State<DateWidget> {
//   DateTime date = DateTime.now();
//   late DateTime? picked;
//   TextEditingController? dateControler;
//   final String hintTextDate;
//
//
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: EdgeInsetsDirectional.only(
//         start: SizeConfig.scaleWidth(20),
//         end: SizeConfig.scaleWidth(20),
//         bottom: SizeConfig.scaleHeight(10),
//       ),
//       child: Column(
//         children: [
//           BudgetPlannerText(
//             text: AppLocalizations.of(context)!.bottomSheet_title,
//             textAlign: TextAlign.center,
//             fontsize: 20,
//             fontWeight: FontWeight.bold,
//           ),
//           TextField(
//             onTap: () async {
//               FocusScope.of(context).requestFocus(new FocusNode());
//
//               picked = await showDatePicker(
//                 context: context,
//                 initialDate: DateTime.now(),
//                 firstDate: DateTime(2019),
//                 lastDate: DateTime(2022),
//               );
//               if (picked != null && picked != date) {
//                 setState(() {
//                   date = picked!;
//                 });
//               }
//             },
//             controller: dateControler,
//             readOnly: true,
//             decoration: InputDecoration(
//               hintText: hintTextDate,
//               hintStyle: TextStyle(
//                 fontSize: 23,
//                 color: Colors.black,
//               ),
//             ),
//           ),
//           Spacer(),
//           BudgetPlannerButton(
//             onPressedPage: () {},
//             textButton: AppLocalizations.of(context)!.bottomSheet_btn_text,
//           )
//         ],
//       ),
//     );
//   }
// }
