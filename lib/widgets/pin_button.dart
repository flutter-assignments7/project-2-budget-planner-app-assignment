import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PinButton extends StatelessWidget {
  final String text;
  final VoidCallback? onPressed;

  PinButton({
    required this.text,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(
        left: SizeConfig.scaleWidth(12),
        right: SizeConfig.scaleWidth(12),
        bottom: SizeConfig.scaleHeight(24),
      ),
      elevation: 6,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(19),
      ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          minimumSize: Size(SizeConfig.scaleWidth(71),SizeConfig.scaleHeight(71)),
          primary: Colors.white,
          onSurface: Colors.red,
        ),
        onPressed: onPressed,
        child: BudgetPlannerText(text: text,fontsize: 23,fontWeight: FontWeight.bold,color: Color(0xFF472FC8),)
      ),
    );
  }
}
