import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';

import 'budget_planner_text.dart';

class BudgetPlannerButton extends StatelessWidget {

  static const Color colors = Color(0xffF472FCB);


  final VoidCallback? onPressedPage;
  final String textButton;
  final Color color;

  BudgetPlannerButton({
    this.onPressedPage,
    required this.textButton,
    this.color = colors,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        minimumSize: Size(double.infinity, 60),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(30)),
        ),
        primary: Color(0xFF472FC8),

      ),
      onPressed: onPressedPage,
      child: BudgetPlannerText(
        text: textButton,
        fontsize: 15,
        fontWeight: FontWeight.bold,
        color: Colors.white,
        textAlign: TextAlign.center,
      ),
    );
  }
}
