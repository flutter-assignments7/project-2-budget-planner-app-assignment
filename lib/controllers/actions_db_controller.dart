
import 'package:budget_planner_app/controllers/db_operations.dart';
import 'package:budget_planner_app/models/actions.dart';
import 'package:budget_planner_app/models/user.dart';
import 'package:budget_planner_app/storage/db_provider.dart';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:sqflite/sqlite_api.dart';

class ActionsDbController with DbOperations<ActionOperation>{
  final Database _database;
  ActionsDbController() : _database = DBProvider().database;

  @override
  Future<int> create(ActionOperation object) async {
    // TODO: implement create
    int value = await _database.insert('actions', object.toMap());
    return value;
  }

  @override
  Future<bool> delete(int id) async {
    // TODO: implement delete
    final result = await _database.delete('actions',where: 'user_id=?',whereArgs: [id]);
    if(result>0){
      return true;
    }else{
      return false;
    }
    // return await _database
    //     .delete('actions', where: 'id = ?', whereArgs: [id]) >
    //     0;
  }

  @override
  Future<List<ActionOperation>> read() async {
    // TODO: implement read
    List<Map<String, Object?>> data = await _database.query('actions',where: 'user_id = ?', whereArgs: [SharedPrefController().id]);
    if(data.isNotEmpty) {
      print('user_id ${SharedPrefController().id}');
      print('NOTES COUNT: ${data.length}');
      return data.map((rowMap) => ActionOperation.fromMap(rowMap)).toList();
      // List<ActionOperation> actions = data.map((contactRowMap) {
      //   return ActionOperation.fromMap(contactRowMap);
      // // }).toList();
      // print("LIST FINAL: ${actions.first.id}");
      // return actions;
    }
    print('action aa: ${data.length}');
    return [];
  }

  @override
  Future<bool> update(ActionOperation object) async {
    // TODO: implement update
    return await _database.update('actions', object.toMap(),
        where: 'id = ?', whereArgs: [object.id]) >
        0;
  }

}