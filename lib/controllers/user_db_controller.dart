
import 'package:budget_planner_app/controllers/db_operations.dart';
import 'package:budget_planner_app/models/user.dart';
import 'package:budget_planner_app/storage/db_provider.dart';
import 'package:sqflite/sqlite_api.dart';

class UserDbController with DbOperations<User>{
  final Database _database;
  UserDbController() : _database = DBProvider().database;


  Future<User?> login({required String email, required int pinCode}) async {
    //SELECT * FROM users WHERE email = email@app AND password = PASSWORD123;
    List<Map<String, dynamic>> data = await _database.query('users',
        where: 'email = ? AND pin = ?', whereArgs: [email, pinCode]);
    if(data.isNotEmpty){
      return data.map((rowMap) => User.fromMap(rowMap)).first;
    }
    return null;
  }


  @override
  Future<int> create(User object) async {
    // TODO: implement create
    int value = await _database.insert('users', object.toMap());
    return value;
  }

  @override
  Future<bool> delete(int id) async {
    // TODO: implement delete
    return await _database
        .delete('users', where: 'id = ?', whereArgs: [id]) >
        0;
  }

  @override
  Future<List<User>> read() async {
    // TODO: implement read
    List<Map<String, Object?>> data = await _database.query('users');
    if (data.isNotEmpty) {
      return data.map((rowMap) => User.fromMap(rowMap)).toList();
    }
    return [];
  }
    // List<User> users = data.map((contactRowMap) {
    //   return User.fromMap(contactRowMap);
    // }).toList();
    //
    // print("LIST FINAL: ${users.first.id}");
    // return users;

  @override
  Future<bool> update(User object) async {
    // TODO: implement update
    return await _database.update('users', object.toMap(),
        where: 'id = ?', whereArgs: [object.id]) >
        0;
  }
  
}