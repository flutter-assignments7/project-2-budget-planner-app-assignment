
import 'package:budget_planner_app/controllers/db_operations.dart';
import 'package:budget_planner_app/models/category.dart';
import 'package:budget_planner_app/models/user.dart';
import 'package:budget_planner_app/storage/db_provider.dart';
import 'package:sqflite/sqlite_api.dart';

class CategoryDbController with DbOperations<Category>{
  final Database _database;
  CategoryDbController() : _database = DBProvider().database;

  @override
  Future<int> create(Category object) async {
    // TODO: implement create
    int value = await _database.insert('categories', object.toMap());
    return value;
  }

  @override
  Future<bool> delete(int id) async {
    // TODO: implement delete
    return await _database
        .delete('categories', where: 'id = ?', whereArgs: [id]) >
        0;
  }

  @override
  Future<List<Category>> read() async {
    // TODO: implement read
    List<Map<String, Object?>> data = await _database.query('categories');
    List<Category> categories = data.map((contactRowMap) {
      return Category.fromMap(contactRowMap);
    }).toList();

    print("LIST FINAL: ${categories.first.id}");
    return categories;
  }

  @override
  Future<bool> update(Category object) async {
    // TODO: implement update
    return await _database.update('categories', object.toMap(),
        where: 'id = ?', whereArgs: [object.id]) >
        0;
  }

}