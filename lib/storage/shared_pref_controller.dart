import 'package:budget_planner_app/models/user.dart';
import 'package:budget_planner_app/storage/pref_Keys.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefController{


  static final SharedPrefController _instance = SharedPrefController._internal();

  late SharedPreferences _sharedPreferences;

  factory SharedPrefController(){
    return _instance;
  }

  SharedPrefController._internal();

  Future<void> initSharedPreferences() async{
    _sharedPreferences = await SharedPreferences.getInstance();
  }

  SharedPreferences get preferences => _sharedPreferences;


  // LOCALIZATOINS
  Future<bool> changeLanguage(String langCode) async {
   return await _sharedPreferences.setString(PrefKeys.LANG_CODE_KEY, langCode);
  }

  String currentLanguage(){
    return _sharedPreferences.getString(PrefKeys.LANG_CODE_KEY) ?? 'en';
  }



  // login user
  Future<void> save(User user) async {
    print("PREF: ${user.id}");
    // print('user data: ${user.pin}');
    await _sharedPreferences.setBool('logged_in', true);
    await _sharedPreferences.setInt('id', user.id);
    await _sharedPreferences.setString('name', user.name);
    await _sharedPreferences.setString('email', user.email);
    await _sharedPreferences.setInt('currency_id', user.currencyId);
    await _sharedPreferences.setDouble('day_limit', user.dayLimit);
    await _sharedPreferences.setInt('pin', user.pin);
    print('user pin data: ${user.pin}');
  }

  int get id => _sharedPreferences.getInt('id') ?? 0;

  bool isLoggedIn(){
    return _sharedPreferences.getBool('logged_in') ?? false;
  }

  Future<bool> logout() async {
    return await _sharedPreferences.clear();
  }


}