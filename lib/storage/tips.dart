class Tip{
  final String title;
  final String body;
  final String date;
  Tip({
    required this.title,
    required this.body,
    required this.date,
  });
  static List<Tip> tips=[
    Tip(
        title: 'How to save a budget 10 tips',
        body: 'What can we tell you about saving money on a tight budget that you don’t already know? You don’t need another source advising you to ditch cable television, use coupons,'
            ' find more people to share housing expenses with, or—God forbid—get a side hustleYou’re already busy and tired, but you’re still motivated to save more money, whether your goal is paying off debt, building your emergency fund, contributing to your IRA, or something else. Let’s try to dig deeper and offer some money-saving tips you might not have thought of yet.',
        date: '2 months ago'

    ),
  ];
}
