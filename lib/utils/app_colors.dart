import 'package:flutter/material.dart';
class AppColors{
  // static const SUBTITLE=Color(0xff8E8E93);
  // static const PASSWORD_CIRCLE_ACTIVE=Color(0xff3DC5FF);
  // static const PASSWORD_CIRCLE_DEFAULT=Color(0xff333437);
  //static const SCAFFOLD_BACKGROUND=Color(0xffC7C7C7);
  static const TITLE=Color(0xff0D0E56);
  static const PROGRESS_BAR=Color(0xffF1F4FF);
  static const PROGRESS_BAR_FILL=Color(0xffF472FCB);
  static const SUBTITLE=Color(0xff787C98);
  static const LOGIN_BUTTON=Color(0xff351DB6);
  static const BN_ICON_DISABLED=Color(0xff979797);
  static const BOTTOM_NAV_BACKGROUND=Color(0xffD3CFEA);
  static const OPERATION_TYPE_BUTTON_TEXT=Color(0xff181819);
  static const SETTING_DANGER=Color(0xffD50000);
  static const HOME_GREEN=Color(0xff00BEA1);
  static const LISTTILE_SUBTITLE=Color(0xff9F9DED);
  static const DATE=Color(0xffB9BACE);
  static const AMOUNT_EXPENSE=Color(0xffF33720);



  // static const LOCK=Color(0xff333437);
  // static const TEXT_FIELD=Color(0xffF1F1F4);
  // static const RECENT_PASSWORD_BACKGROUND = Color(0xffF2F4F7);
  // static const SETTING_YELLOW=Color(0xffFFD82B);
  // static const SETTING_PURPLE= Color(0xffBA8EFF);
  // static const SETTING_GREEN = Color(0xff3DE08F);



}