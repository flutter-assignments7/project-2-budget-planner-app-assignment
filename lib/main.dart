import 'package:budget_planner_app/models/language_change_notifier.dart';
import 'package:budget_planner_app/notifiers/actions_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/category_provider_notifier.dart';
import 'package:budget_planner_app/notifiers/currencyDetails.dart';
import 'package:budget_planner_app/notifiers/user_provider_notifier.dart';
import 'package:budget_planner_app/screens/all_action_screen.dart';
import 'package:budget_planner_app/screens/auth/create_account_screen.dart';
import 'package:budget_planner_app/screens/auth/create_account_success.dart';
import 'package:budget_planner_app/screens/auth/login_screen.dart';
import 'package:budget_planner_app/screens/auth/pin_code_screen.dart';
import 'package:budget_planner_app/screens/bottom_navegationBar/add_new_category.dart';
import 'package:budget_planner_app/screens/bottom_navegationBar/add_new_operation.dart';
import 'package:budget_planner_app/screens/bottom_navegationBar/main_screen.dart';
import 'package:budget_planner_app/screens/bottom_navegationBar/new_operation_success.dart';
import 'package:budget_planner_app/screens/bottom_navegationBar/tips_details_screen.dart';
import 'package:budget_planner_app/screens/categories_screen2.dart';
import 'package:budget_planner_app/screens/currency_select_screen.dart';
import 'package:budget_planner_app/screens/details_screen.dart';
import 'package:budget_planner_app/screens/on_boarding/on_boarding_screen.dart';
import 'package:budget_planner_app/screens/settings_screens/about_screen.dart';
import 'package:budget_planner_app/screens/settings_screens/settings_screen.dart';
import 'package:budget_planner_app/screens/splash_screen.dart';
import 'package:budget_planner_app/storage/db_provider.dart';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPrefController().initSharedPreferences();
  await DBProvider().initDatabase();
  runApp(MainApp());
}


class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => LanguageChangeNotifier(
            Locale(SharedPrefController().currentLanguage()),
          ),
        ),
        ChangeNotifierProvider<CurrencyList>(create: (context) => CurrencyList()),
        ChangeNotifierProvider<UserProviderNotifier>(create: (context) => UserProviderNotifier()),
        ChangeNotifierProvider<CategoryProviderNotifier>(create: (context) => CategoryProviderNotifier()),
        ChangeNotifierProvider<ActionsProviderNotifier>(create: (context) => ActionsProviderNotifier()),
      ],
      child: MaterialMainApp(),
    );
  }
}


class MaterialMainApp extends StatelessWidget {
  const MaterialMainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
          debugShowCheckedModeBanner: false,

          // localizationsDelegates: AppLocalizations.localizationsDelegates,
          // supportedLocales: AppLocalizations.supportedLocales,

          localizationsDelegates: [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: [
            Locale('ar', ''),
            Locale('en', ''),
          ],
          locale: Provider.of<LanguageChangeNotifier>(context).locale,

          initialRoute: '/splash_screen',
          routes: {
            '/splash_screen': (context) => SplashScreen(),
            '/on_boarding_screen': (context) => OnBoardingScreen(),

            '/login_screen': (context) => LoginScreen(),
            '/create_account_screen': (context) => CreateAccountScreen(),
            '/pin_code_screen': (context) => PinCodeScreen(),
            '/create_account_success': (context) => CreateAccountSuccess(),
            '/currency_select_screen': (context) => CurrencySelectScreen(),

            '/main_screen': (context) => MainScreen(),
            '/tips_details_screen': (context) => TipsDetailsScreen(),

            '/all_actions_screen': (context) => AllActionScreen(),

            '/add_new_category': (context) => AddNewCategory(),
            '/add_new_operation': (context) => AddNewOperation(),
            '/new_operation_success': (context) => NewOperationSuccess(),

            // '/details_screen': (context) => DetailsScreen(),

            '/settings_screen': (context) => SettingsScreen(),
            '/about_app_screen': (context) => AboutAppScreen(),

            '/categories_screen_2': (context) => CategoriesScreen2(),


            // '/currency_select_screen': (context) => CurrencySelectScreen(),


          },
        );
  }
}


// class _MainAppState extends State<MainApp> {
//
//
//   @override
//   Widget build(BuildContext context) {
//     return MultiProvider(
//       providers: [
//         ChangeNotifierProvider(
//           create: (context) => LanguageChangeNotifier(
//             Locale(SharedPrefController().currentLanguage()),
//           ),
//         ),
//       ],
//       child: Consumer<LanguageChangeNotifier>
//         (builder: (BuildContext context, LanguageChangeNotifier languageChangeNotifier, child){
//         return MaterialApp(
//           debugShowCheckedModeBanner: false,
//
//           // localizationsDelegates: AppLocalizations.localizationsDelegates,
//           // supportedLocales: AppLocalizations.supportedLocales,
//
//           localizationsDelegates: [
//             AppLocalizations.delegate,
//             GlobalMaterialLocalizations.delegate,
//             GlobalWidgetsLocalizations.delegate,
//             GlobalCupertinoLocalizations.delegate,
//           ],
//           supportedLocales: [
//             Locale('ar', ''),
//             Locale('en', ''),
//           ],
//           // locale: Provider.of<LanguageChangeNotifier>(context).locale,
//           locale: languageChangeNotifier.locale,
//
//           initialRoute: '/splash_screen',
//           routes: {
//             '/splash_screen': (context) => SplashScreen(),
//           },
//         );
//       },),
//     );
//   }
// }
