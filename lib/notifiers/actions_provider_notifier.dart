import 'package:budget_planner_app/controllers/actions_db_controller.dart';
import 'package:budget_planner_app/models/actions.dart';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:flutter/material.dart';

class ActionsProviderNotifier extends ChangeNotifier{

  late ActionsDbController _actionsDbController;
  List<ActionOperation> actions = <ActionOperation>[];

  ActionsProviderNotifier() {
    _actionsDbController = ActionsDbController();
  }

  Future create(ActionOperation actionOperation) async {
    // TODO: implement create
    int id = await _actionsDbController.create(actionOperation);
    if (id != 0) {
      actionOperation.id = id;
      actions.add(actionOperation);
      notifyListeners();
      return true;
    }
    return false;
  }

  Future delete(int id) async {
    // TODO: implement delete
    bool deleted = await _actionsDbController.delete(id);
    if (deleted) {
      actions.removeWhere((element) => element.id == id);
      actions = [];
      notifyListeners();
    }
  }

  Future read() async {
    // TODO: implement read
    actions = await _actionsDbController.read();
    sort();
    print('user_id: ${SharedPrefController().id}');
    print('SIZE: ${actions.length}');

    notifyListeners();
  }

  Future update(int id, ActionOperation object) async {
    // TODO: implement update
    bool updated = await _actionsDbController.update(object);
    if (updated) {
      //
      int index = actions.indexWhere((element) => element.id == object.id);
      actions[index] = object;
      notifyListeners();
    }
  }

  List<ActionOperation> get todayOperations{
    List<ActionOperation> x=[];
    actions.forEach((element) {
      if(
      element.date.year==DateTime.now().year &&
          element.date.month==DateTime.now().month &&
          element.date.day==DateTime.now().day){
        x.add(element);
      }
    });
    return x;
  }

  ActionOperation getById(int id){
    return actions.firstWhere((element) => element.id == id);
  }

  // Future<void> get(int id)async{
  //   actions = await _actionsDbController.get(id);
  //   // actions = data.map((e)=>actions.fromMap(e)).toList();
  //   sort();
  //   notifyListeners();
  // }
  void sort(){
    actions.sort((a, b) => b.date.compareTo(a.date));
  }

}