import 'package:budget_planner_app/controllers/db_operations.dart';
import 'package:budget_planner_app/controllers/user_db_controller.dart';
import 'package:budget_planner_app/models/user.dart';
import 'package:budget_planner_app/storage/db_provider.dart';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:flutter/material.dart';

class UserProviderNotifier extends ChangeNotifier{


  List<User> users = <User>[];
  UserDbController _userDbController = UserDbController();



  Future<bool>login({required User userData})async{
    User? user = await _userDbController.login(email: userData.email, pinCode: userData.pin);
    if (user != null) {
      print('USER ID: ${user.id}');
      // print('user data: ${user.pin}');
      await SharedPrefController().save(user);
      notifyListeners();
      return true;
    }
    return false;

    // final mapUsers = await _db.query('users',where: 'email=?',whereArgs: [email]);
    // if(mapUsers.isNotEmpty){
    //   user = mapUsers.map((e) => User.fromMap(e)).toList()[0];
    //   if(user.pin==pin){
    //     users.add(user);
    //     notifyListeners();
    //     return user.id;
    //   }else{
    //     return 0;
    //   }
    // }else{
    //   return -1;
    // }
  }


  Future<bool> create(User user) async {
    int id = await _userDbController.create(user);
    if (id != 0) {
      user.id = id;
      users.add(user);
      await SharedPrefController().save(user);
      notifyListeners();
      return true;
    }
    return false;
  }

  Future<bool> delete(int id) async {
    bool deleted = await _userDbController.delete(id);
    if (deleted) {
      int index = users.indexWhere((element) => element.id == id);
      if(index != -1) {
        users.removeAt(index);
        notifyListeners();
        return true;
      }
    }
    return false;
  }

  Future read() async {
      users = await _userDbController.read();
      notifyListeners();
  }

  Future<bool> update(User updatedUser) async {
    bool updated = await _userDbController.update(updatedUser);
    if (updated) {
      int index = users.indexWhere((element) => element.id == updatedUser.id);
      if(index != -1) {
        users[index] = updatedUser;
        notifyListeners();
        return true;
      }
    }
    return false;
  }
  // Future<bool> removeUser(int id)async{
  //   final result = await _userDbController.delete('users',where: 'id=?',whereArgs: [id]);
  //   return result>0;
  // }

}