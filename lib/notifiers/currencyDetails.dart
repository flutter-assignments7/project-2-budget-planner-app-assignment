import 'package:budget_planner_app/models/actions.dart';
import 'package:budget_planner_app/models/currency.dart';
import 'package:budget_planner_app/storage/db_provider.dart';
import 'package:flutter/foundation.dart';

class Currencies{

 final String name;
 final String symbol;
 final String img;
 bool isSelected;
 int id;


 Currencies({
   required this.name,
   required this.symbol,
   required this.img,
   required this.id,
   this.isSelected=false,
});
}

class CurrencyList with ChangeNotifier{
   List<Currencies> cList=[
     Currencies(name: 'US Dollar',symbol: '\$' , img: "images/US Dollar.png",id: 2),
     Currencies(name: 'Jordanian Dinar',symbol:'JOD', img: "images/Jordanian Dinar.png",id:3),
     Currencies(name: 'Shekel',symbol: '₪' , img: "images/Shekel.png",id:1),
  ];

   final _db = DBProvider().database;


   void selectCurrency(String name){
     cList.forEach((element) {
       element.isSelected = false;
     });
     final index = cList.indexWhere((element) => element.name==name);
     cList[index].isSelected=true;
     notifyListeners();
   }

   int getSelectedIndex(){
     final index = cList.indexWhere((element) => element.isSelected);
     return index;
   }

   void clearSelected(){
     cList.forEach((element) {
       element.isSelected = false;
     });
     notifyListeners();
   }

   int getSelectedCurrencyId(){
     final index = cList.indexWhere((element) => element.isSelected);
     if(index>=0){
       final c =  cList[index];
       return c.id;
     }
     return 0;
   }

   String getSelectedCurrencyName(){
     final index = cList.indexWhere((element) => element.isSelected);
     if(index>=0){
       return cList[index].name;
     }else{
       return '';
     }
   }
   String getSelectedCurrencyNameAndSymbol(){
     final index = cList.indexWhere((element) => element.isSelected);
     if(index>=0){
       return '${cList[index].name} (${cList[index].symbol})';
     }else{
       return '';
     }
   }

   String getById(int id){
     return cList.firstWhere((element) => element.id==id).symbol;
   }
   String getById2(int id){
     return cList.firstWhere((element) => element.id==id).name;
   }

   Future<void> get()async{
     final data = await _db.query('currencies');
     cList = data.map((e) =>
         Currency(id: e['id'] as int ,
             nameAr: e['name_ar'].toString(),
             nameEn: e['name_en'].toString())).cast<Currencies>().toList();
     notifyListeners();
   }

   String getFullCurrencyName(String currencyName){
     if(currencyName == 'US Dollar') {
         return 'United States Dollar (USD \$)';
       }else if(currencyName == 'Euro'){
       return 'Euro (EUR €)';
     }else if (currencyName == 'Jordanian Dinar'){
       return 'Jordanian Dinar (JOD)';
     }else if(currencyName == 'Shekel'){
       return 'New Israeli Shekel  (ILS ₪)';
     }else {
       return '';
     }
     }

   double convertOperations(int userCurrencyId,ActionOperation actionOperation){
     double result = 0;
     if(userCurrencyId==actionOperation.currencyId) {
       result = actionOperation.amount.toDouble();
     }else{
       if(userCurrencyId==1){
         if(actionOperation.currencyId==2)
           result = actionOperation.amount * 3.27;
         if(actionOperation.currencyId==3) result=actionOperation.amount*4.62;
       }
       if(userCurrencyId==2){
         if(actionOperation.currencyId==1) result=actionOperation.amount*0.31;
         if(actionOperation.currencyId==3) result=actionOperation.amount*1.41;
       }
       if(userCurrencyId==3){
         if(actionOperation.currencyId==1) result=actionOperation.amount*0.22;
         if(actionOperation.currencyId==2) result=actionOperation.amount*0.71;
       }
     }
     return result;
   }


}