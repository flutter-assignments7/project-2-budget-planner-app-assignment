import 'package:budget_planner_app/controllers/category_db_controller.dart';
import 'package:budget_planner_app/models/category.dart';
import 'package:budget_planner_app/storage/shared_pref_controller.dart';
import 'package:flutter/material.dart';

class CategoryProviderNotifier extends ChangeNotifier{

  late CategoryDbController _categoryDbController;
  List<Category> categories = <Category>[];

  CategoryProviderNotifier() {
    _categoryDbController = CategoryDbController();
    read();
  }

  Future<bool> create(Category category) async {
    // TODO: implement create
    int id = await _categoryDbController.create(category);
    if (id != 0) {
      category.id = id;
      categories.add(category);
      notifyListeners();
      return true;
    }
    return false;
  }

  Future delete(int id) async {
    // TODO: implement delete
    bool deleted = await _categoryDbController.delete(id);
    if (deleted) {
      categories.removeWhere((element) => element.id == id);
      notifyListeners();
    }
  }

  Future read() async {
    // TODO: implement read
    categories = await _categoryDbController.read();
    print('SIZE: ${categories.length}');
    notifyListeners();
  }

  Future update(int id, Category object) async {
    // TODO: implement update
    bool updated = await _categoryDbController.update(object);
    if (updated) {
      //
      int index = categories.indexWhere((element) => element.id == object.id);
      categories[index] = object;
      notifyListeners();
    }
  }

  String getCategoryNameById(int id){
    return categories.firstWhere((element) => element.id==id).name;
  }
}